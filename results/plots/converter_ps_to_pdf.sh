#! /bin/bash

for FILE in $(ls /home/cleyton/news_shock_project/plots/*.ps)
do    
    ps2pdf -sPAPERSIZE=a4 -dOptimize=true -dEmbedAllFonts=true $FILE
done
