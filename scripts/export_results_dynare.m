%----------------------------------------------------------------------------------
% PROGRAM: export_results_dynare.m
%
% LANGUAGE: octave/Matlab
% 
% DESCRIPTION: This file extracts IRF's, conditional FEVD and shock
%              decomposition results from the results of dynare's
%              estimation.
% 
% AUTHOR: Marcelo Eduardo 
%
% MODIFIED BY: Cleyton Farias
% 
% LAST MODIFIED: October 07, 2020
%
% NOTE: The directory address must be modified.
%----------------------------------------------------------------------------------


close all
dynare master.mod; 


{

% Commodity Price Shock: 
P_C.eps_CN0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_C_news0;
r.eps_CN0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_C_news0;
Y.eps_CN0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_C_news0;
C.eps_CN0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_C_news0;
I.eps_CN0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_C_news0;
tb_y.eps_CN0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_C_news0;
L.eps_CN0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_C_news0;
D.eps_CN0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_C_news0;
reer.eps_CN0 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_C_news0; 
AT.eps_CN0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_C_news0;
AN.eps_CN0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_C_news0;
AC.eps_CN0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_C_news0;

P_C.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_C_news0;
r.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_C_news0;
Y.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_C_news0;
C.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_C_news0;
I.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_C_news0;
tb_y.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_C_news0;
L.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_C_news0;
D.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_C_news0;
reer.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_C_news0;
AT.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_C_news0;
AN.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_C_news0;
AC.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_C_news0;

P_C.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_C_news0;
r.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_C_news0;
Y.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_C_news0;
C.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_C_news0;
I.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_C_news0;
tb_y.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_C_news0;
L.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_C_news0;
D.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_C_news0;
reer.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_C_news0;
AT.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_C_news0;
AN.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_C_news0;
AC.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_C_news0;

 

% News Shocks 2 periods in advance: 
P_C.eps_CN2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_C_news2;
r.eps_CN2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_C_news2;
Y.eps_CN2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_C_news2;
C.eps_CN2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_C_news2;
I.eps_CN2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_C_news2;
tb_y.eps_CN2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_C_news2;
L.eps_CN2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_C_news2;
D.eps_CN2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_C_news2;
reer.eps_CN2 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_C_news2; 
AT.eps_CN2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_C_news2;
AN.eps_CN2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_C_news2;
AC.eps_CN2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_C_news2;

P_C.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_C_news2;
r.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_C_news2;
Y.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_C_news2;
C.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_C_news2;
I.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_C_news2;
tb_y.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_C_news2;
L.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_C_news2;
reer.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_C_news2; 
D.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_C_news2;
AT.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_C_news2;
AN.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_C_news2;
AC.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_C_news2;

P_C.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_C_news2;
r.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_C_news2;
Y.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_C_news2;
C.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_C_news2;
I.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_C_news2;
tb_y.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_C_news2;
L.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_C_news2;
D.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_C_news2;
reer.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_C_news2; 
AT.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_C_news2;
AN.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_C_news2;
AC.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_C_news2;


 
% News Shocks 4 periods in advance: 
P_C.eps_CN4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_C_news4;
r.eps_CN4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_C_news4;
Y.eps_CN4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_C_news4;
C.eps_CN4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_C_news4;
I.eps_CN4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_C_news4;
tb_y.eps_CN4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_C_news4;
L.eps_CN4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_C_news4;
D.eps_CN4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_C_news4;
reer.eps_CN4 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_C_news4; 
AT.eps_CN4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_C_news4;
AN.eps_CN4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_C_news4;
AC.eps_CN4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_C_news4;

P_C.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_C_news4;
r.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_C_news4;
Y.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_C_news4;
C.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_C_news4;
I.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_C_news4;
tb_y.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_C_news4;
L.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_C_news4;
D.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_C_news4;
reer.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_C_news4; 
AT.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_C_news4;
AN.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_C_news4;
AC.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_C_news4;

P_C.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_C_news4;
r.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_C_news4;
Y.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_C_news4;
C.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_C_news4;
I.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_C_news4;
tb_y.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_C_news4;
L.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_C_news4;
D.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_C_news4;
reer.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_C_news4; 
AT.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_C_news4;
AN.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_C_news4;
AC.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_C_news4;




 % Interest rate shock: 
P_C.eps_r = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_r;
r.eps_r  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_r;
Y.eps_r = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_r;
C.eps_r = oo_.PosteriorIRF.dsge.Median.C_hat_eps_r;
I.eps_r = oo_.PosteriorIRF.dsge.Median.I_hat_eps_r;
tb_y.eps_r = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_r;
L.eps_r = oo_.PosteriorIRF.dsge.Median.L_hat_eps_r;
D.eps_r = oo_.PosteriorIRF.dsge.Median.D_hat_eps_r;
reer.eps_r = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_r; 
AT.eps_r = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_r;
AN.eps_r = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_r;
AC.eps_r = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_r;

P_C.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_r;
r.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_r;
Y.eps_r_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_r;
C.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_r;
I.eps_r_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_r;
tb_y.eps_r_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_r;
L.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_r;
D.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_r;
reer.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_r; 
AT.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_r;
AN.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_r;
AC.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_r;

P_C.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_r;
r.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_r;
Y.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_r;
C.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_r;
I.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_r;
tb_y.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_r;
L.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_r;
D.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_r;
reer.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_r; 
AT.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_r;
AN.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_r;
AC.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_r;






% unanticipated Shock productivity Tradable sector: 
P_C.eps_AT0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AT_news0;
r.eps_AT0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AT_news0;
Y.eps_AT0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AT_news0;
C.eps_AT0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AT_news0;
I.eps_AT0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AT_news0;
tb_y.eps_AT0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AT_news0;
L.eps_AT0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AT_news0;
D.eps_AT0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AT_news0;
reer.eps_AT0 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AT_news0;
AT.eps_AT0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AT_news0;
AN.eps_AT0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AT_news0;
AC.eps_AT0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AT_news0;
 
P_C.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AT_news0;
r.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AT_news0;
Y.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AT_news0;
C.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AT_news0;
I.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AT_news0;
tb_y.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AT_news0;
L.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AT_news0;
D.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AT_news0;
reer.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AT_news0; 
AT.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AT_news0;
AN.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AT_news0;
AC.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AT_news0;

P_C.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AT_news0;
r.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AT_news0;
Y.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AT_news0;
C.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AT_news0;
I.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AT_news0;
tb_y.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AT_news0;
L.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AT_news0;
D.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AT_news0;
reer.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AT_news0; 
AT.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AT_news0;
AN.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AT_news0;
AC.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AT_news0;




 
% News with 2 periods in advance productivity Tradable sector: 
P_C.eps_AT2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AT_news2;
r.eps_AT2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AT_news2;
Y.eps_AT2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AT_news2;
C.eps_AT2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AT_news2;
I.eps_AT2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AT_news2;
tb_y.eps_AT2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AT_news2;
L.eps_AT2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AT_news2;
D.eps_AT2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AT_news2;
reer.eps_AT2 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AT_news2; 
AT.eps_AT2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AT_news2;
AN.eps_AT2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AT_news2;
AC.eps_AT2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AT_news2;
 
P_C.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AT_news2;
r.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AT_news2;
Y.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AT_news2;
C.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AT_news2;
I.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AT_news2;
tb_y.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AT_news2;
L.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AT_news2;
D.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AT_news2;
reer.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AT_news2;  
AT.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AT_news2;
AN.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AT_news2;
AC.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AT_news2;

P_C.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AT_news2;
r.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AT_news2;
Y.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AT_news2;
C.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AT_news2;
I.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AT_news2;
tb_y.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AT_news2;
L.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AT_news2;
D.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AT_news2;
reer.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AT_news2; 
AT.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AT_news2;
AN.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AT_news2;
AC.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AT_news2;


 
 
% News with 4 periods in advance productivity Tradable sector: 
P_C.eps_AT4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AT_news4;
r.eps_AT4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AT_news4;
Y.eps_AT4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AT_news4;
C.eps_AT4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AT_news4;
I.eps_AT4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AT_news4;
tb_y.eps_AT4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AT_news4;
L.eps_AT4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AT_news4;
D.eps_AT4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AT_news4;
reer.eps_AT4 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AT_news4;  
AT.eps_AT4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AT_news4;
AN.eps_AT4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AT_news4;
AC.eps_AT4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AT_news4;
 
P_C.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AT_news4;
r.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AT_news4;
Y.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AT_news4;
C.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AT_news4;
I.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AT_news4;
tb_y.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AT_news4;
L.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AT_news4;
D.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AT_news4;
reer.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AT_news4;  
AT.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AT_news4;
AN.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AT_news4;
AC.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AT_news4;

P_C.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AT_news4;
r.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AT_news4;
Y.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AT_news4;
C.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AT_news4;
I.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AT_news4;
tb_y.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AT_news4;
L.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AT_news4;
D.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AT_news4;
reer.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AT_news4;
AT.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AT_news4;
AN.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AT_news4;
AC.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AT_news4;




 

% Shock productivity NonTradable sector: 
P_C.eps_AN0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AN_news0;
r.eps_AN0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AN_news0;
Y.eps_AN0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AN_news0;
C.eps_AN0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AN_news0;
I.eps_AN0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AN_news0;
tb_y.eps_AN0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AN_news0;
L.eps_AN0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AN_news0;
D.eps_AN0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AN_news0;
reer.eps_AN0 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AN_news0;  
AT.eps_AN0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AN_news0;
AN.eps_AN0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AN_news0;
AC.eps_AN0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AN_news0;

P_C.eps_AN0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AN_news0;
r.eps_AN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AN_news0;
Y.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AN_news0;
C.eps_AN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AN_news0;
I.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AN_news0;
tb_y.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AN_news0;
L.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AN_news0;
D.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AN_news0;
reer.eps_AN0_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AN_news0;
AT.eps_AN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AN_news0;
AN.eps_AN0_inf  = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AN_news0;
AC.eps_AN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AN_news0;

P_C.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AN_news0;
r.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AN_news0;
Y.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AN_news0;
C.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AN_news0;
I.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AN_news0;
tb_y.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AN_news0;
L.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AN_news0;
D.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AN_news0;
reer.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AN_news0;
AT.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AN_news0;
AN.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AN_news0;
AC.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AN_news0;




 
% News with 2 periods in advance productivity NonTradable sector: 
P_C.eps_AN2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AN_news2;
r.eps_AN2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AN_news2;
Y.eps_AN2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AN_news2;
C.eps_AN2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AN_news2;
I.eps_AN2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AN_news2;
tb_y.eps_AN2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AN_news2;
L.eps_AN2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AN_news2;
D.eps_AN2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AN_news2;
reer.eps_AN2 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AN_news2;
AT.eps_AN2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AN_news2;
AN.eps_AN2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AN_news2;
AC.eps_AN2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AN_news2;

P_C.eps_AN2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AN_news2;
r.eps_AN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AN_news2;
Y.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AN_news2;
C.eps_AN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AN_news2;
I.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AN_news2;
tb_y.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AN_news2;
L.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AN_news2;
D.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AN_news2;
reer.eps_AN2_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AN_news2;
AT.eps_AN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AN_news2;
AN.eps_AN2_inf  = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AN_news2;
AC.eps_AN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AN_news2;

P_C.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AN_news2;
r.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AN_news2;
Y.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AN_news2;
C.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AN_news2;
I.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AN_news2;
tb_y.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AN_news2;
L.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AN_news2;
D.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AN_news2;
reer.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AN_news2;
AT.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AN_news2;
AN.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AN_news2;
AC.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AN_news2;




 
% News with 4 periods in advance productivity NonTradable sector: 
P_C.eps_AN4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AN_news4;
r.eps_AN4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AN_news4;
Y.eps_AN4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AN_news4;
C.eps_AN4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AN_news4;
I.eps_AN4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AN_news4;
tb_y.eps_AN4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AN_news4;
L.eps_AN4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AN_news4;
D.eps_AN4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AN_news4;
reer.eps_AN4 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AN_news4;
AT.eps_AN4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AN_news4;
AN.eps_AN4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AN_news4;
AC.eps_AN4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AN_news4;

P_C.eps_AN4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AN_news4;
r.eps_AN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AN_news4;
Y.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AN_news4;
C.eps_AN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AN_news4;
I.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AN_news4;
tb_y.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AN_news4;
L.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AN_news4;
D.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AN_news4;
reer.eps_AN4_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AN_news4;
AT.eps_AN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AN_news4;
AN.eps_AN4_inf  = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AN_news4;
AC.eps_AN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AN_news4;

P_C.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AN_news4;
r.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AN_news4;
Y.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AN_news4;
C.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AN_news4;
I.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AN_news4;
tb_y.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AN_news4;
L.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AN_news4;
D.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AN_news4;
reer.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AN_news4;
AT.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AN_news4;
AN.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AN_news4;
AC.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AN_news4;








 
% Shock productivity Commodity sector: 
P_C.eps_AC0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AC_news0;
r.eps_AC0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AC_news0;
Y.eps_AC0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AC_news0;
C.eps_AC0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AC_news0;
I.eps_AC0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AC_news0;
tb_y.eps_AC0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AC_news0;
L.eps_AC0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AC_news0;
D.eps_AC0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AC_news0;
reer.eps_AC0 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AC_news0;
AT.eps_AC0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AC_news0;
AN.eps_AC0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AC_news0;
AC.eps_AC0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AC_news0;
 
P_C.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AC_news0;
r.eps_AC0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AC_news0;
Y.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AC_news0;
C.eps_AC0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AC_news0;
I.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AC_news0;
tb_y.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AC_news0;
L.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AC_news0;
D.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AC_news0;
reer.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AC_news0;
AT.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AC_news0;
AN.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AC_news0;
AC.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AC_news0;
 
P_C.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AC_news0;
r.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AC_news0;
Y.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AC_news0;
C.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AC_news0;
I.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AC_news0;
tb_y.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AC_news0;
L.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AC_news0;
D.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AC_news0;
reer.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AC_news0;
AT.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AC_news0;
AN.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AC_news0;
AC.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AC_news0;





 
% News Shock productivity Commodity sector with 2 periods in advance: 
P_C.eps_AC2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AC_news2;
r.eps_AC2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AC_news2;
Y.eps_AC2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AC_news2;
C.eps_AC2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AC_news2;
I.eps_AC2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AC_news2;
tb_y.eps_AC2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AC_news2;
L.eps_AC2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AC_news2;
D.eps_AC2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AC_news2;
reer.eps_AC2 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AC_news2;
AT.eps_AC2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AC_news2;
AN.eps_AC2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AC_news2;
AC.eps_AC2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AC_news2;
 
P_C.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AC_news2;
r.eps_AC2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AC_news2;
Y.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AC_news2;
C.eps_AC2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AC_news2;
I.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AC_news2;
tb_y.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AC_news2;
L.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AC_news2;
D.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AC_news2;
reer.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AC_news2;
AT.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AC_news2;
AN.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AC_news2;
AC.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AC_news2;
 
P_C.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AC_news2;
r.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AC_news2;
Y.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AC_news2;
C.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AC_news2;
I.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AC_news2;
tb_y.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AC_news2;
L.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AC_news2;
D.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AC_news2;
reer.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AC_news2;
AT.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AC_news2;
AN.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AC_news2;
AC.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AC_news2;
 





% News Shock productivity Commodity sector with 4 periods in advance: 
P_C.eps_AC4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AC_news4;
r.eps_AC4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AC_news4;
Y.eps_AC4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AC_news4;
C.eps_AC4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AC_news4;
I.eps_AC4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AC_news4;
tb_y.eps_AC4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AC_news4;
L.eps_AC4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AC_news4;
D.eps_AC4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AC_news4;
reer.eps_AC4 = oo_.PosteriorIRF.dsge.Median.reer_hat_eps_AC_news4;
AT.eps_AC4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AC_news4;
AN.eps_AC4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AC_news4;
AC.eps_AC4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AC_news4;
 
P_C.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AC_news4;
r.eps_AC4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AC_news4;
Y.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AC_news4;
C.eps_AC4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AC_news4;
I.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AC_news4;
tb_y.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AC_news4;
L.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AC_news4;
D.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AC_news4;
reer.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.reer_hat_eps_AC_news4;
AT.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AC_news4;
AN.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AC_news4;
AC.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AC_news4;
 
P_C.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AC_news4;
r.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AC_news4;
Y.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AC_news4;
C.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AC_news4;
I.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AC_news4;
tb_y.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AC_news4;
L.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AC_news4;
D.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AC_news4;
reer.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.reer_hat_eps_AC_news4;
AT.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AC_news4;
AN.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AC_news4;
AC.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AC_news4;
 





IRF_CN0 = [Y.eps_CN0  Y.eps_CN0_inf Y.eps_CN0_sup C.eps_CN0 C.eps_CN0_inf C.eps_CN0_sup I.eps_CN0  I.eps_CN0_inf I.eps_CN0_sup  tb_y.eps_CN0  tb_y.eps_CN0_inf tb_y.eps_CN0_sup	 L.eps_CN0  L.eps_CN0_inf L.eps_CN0_sup	 D.eps_CN0 D.eps_CN0_inf D.eps_CN0_sup reer.eps_CN0 reer.eps_CN0_inf reer.eps_CN0_sup P_C.eps_CN0 P_C.eps_CN0_inf P_C.eps_CN0_sup r.eps_CN0 r.eps_CN0_inf r.eps_CN0_sup AT.eps_CN0 AT.eps_CN0_inf AT.eps_CN0_sup AN.eps_CN0 AN.eps_CN0_inf AN.eps_CN0_sup AC.eps_CN0 AC.eps_CN0_inf AC.eps_CN0_sup];

IRF_CN2 = [Y.eps_CN2  Y.eps_CN2_inf Y.eps_CN2_sup C.eps_CN2 C.eps_CN2_inf C.eps_CN2_sup I.eps_CN2  I.eps_CN2_inf I.eps_CN2_sup tb_y.eps_CN2  tb_y.eps_CN2_inf tb_y.eps_CN2_sup L.eps_CN2  L.eps_CN2_inf L.eps_CN2_sup D.eps_CN2 D.eps_CN2_inf D.eps_CN2_sup reer.eps_CN2 reer.eps_CN2_inf reer.eps_CN2_sup  P_C.eps_CN2 P_C.eps_CN2_inf P_C.eps_CN2_sup r.eps_CN2 r.eps_CN2_inf r.eps_CN2_sup AT.eps_CN2 AT.eps_CN2_inf AT.eps_CN2_sup AN.eps_CN2 AN.eps_CN2_inf AN.eps_CN2_sup AC.eps_CN2 AC.eps_CN2_inf AC.eps_CN2_sup];
 
IRF_CN4 = [Y.eps_CN4  Y.eps_CN4_inf Y.eps_CN4_sup C.eps_CN4 C.eps_CN4_inf C.eps_CN4_sup I.eps_CN4  I.eps_CN4_inf I.eps_CN4_sup tb_y.eps_CN4  tb_y.eps_CN4_inf tb_y.eps_CN4_sup L.eps_CN4  L.eps_CN4_inf L.eps_CN4_sup D.eps_CN4 D.eps_CN4_inf D.eps_CN4_sup reer.eps_CN4 reer.eps_CN4_inf reer.eps_CN4_sup P_C.eps_CN4 P_C.eps_CN4_inf P_C.eps_CN4_sup r.eps_CN4 r.eps_CN4_inf r.eps_CN4_sup AT.eps_CN4 AT.eps_CN4_inf AT.eps_CN4_sup AN.eps_CN4 AN.eps_CN4_inf AN.eps_CN4_sup AC.eps_CN4 AC.eps_CN4_inf AC.eps_CN4_sup];



IRF_r = [Y.eps_r  Y.eps_r_inf Y.eps_r_sup C.eps_r  C.eps_r_inf C.eps_r_sup I.eps_r  I.eps_r_inf I.eps_r_sup tb_y.eps_r  tb_y.eps_r_inf tb_y.eps_r_sup L.eps_r  L.eps_r_inf L.eps_r_sup D.eps_r D.eps_r_inf D.eps_r_sup reer.eps_r reer.eps_r_inf reer.eps_r_sup P_C.eps_r P_C.eps_r_inf P_C.eps_r_sup r.eps_r r.eps_r_inf r.eps_r_sup AT.eps_r AT.eps_r_inf AT.eps_r_sup AN.eps_r AN.eps_r_inf AN.eps_r_sup AC.eps_r AC.eps_r_inf AC.eps_r_sup];
 

 
IRF_AT0 = [Y.eps_AT0  Y.eps_AT0_inf  Y.eps_AT0_sup C.eps_AT0 C.eps_AT0_inf  C.eps_AT0_sup  I.eps_AT0  I.eps_AT0_inf  I.eps_AT0_sup tb_y.eps_AT0   tb_y.eps_AT0_inf   tb_y.eps_AT0_sup    L.eps_AT0 L.eps_AT0_inf   L.eps_AT0_sup   D.eps_AT0  D.eps_AT0_inf D.eps_AT0_sup  reer.eps_AT0  reer.eps_AT0_inf reer.eps_AT0_sup  P_C.eps_AT0  P_C.eps_AT0_inf   P_C.eps_AT0_sup r.eps_AT0  r.eps_AT0_inf  r.eps_AT0_sup   AT.eps_AT0  AT.eps_AT0_inf AT.eps_AT0_sup  AN.eps_AT0  AN.eps_AT0_inf  AN.eps_AT0_sup AC.eps_AT0 AC.eps_AT0_inf  AC.eps_AT0_sup];

IRF_AT2 = [Y.eps_AT2  Y.eps_AT2_inf Y.eps_AT2_sup C.eps_AT2  C.eps_AT2_inf C.eps_AT2_sup I.eps_AT2  I.eps_AT2_inf I.eps_AT2_sup tb_y.eps_AT2  tb_y.eps_AT2_inf tb_y.eps_AT2_sup L.eps_AT2  L.eps_AT2_inf L.eps_AT2_sup D.eps_AT2 D.eps_AT2_inf D.eps_AT2_sup reer.eps_AT2 reer.eps_AT2_inf reer.eps_AT2_sup P_C.eps_AT2 P_C.eps_AT2_inf P_C.eps_AT2_sup r.eps_AT2 r.eps_AT2_inf r.eps_AT2_sup AT.eps_AT2 AT.eps_AT2_inf AT.eps_AT2_sup AN.eps_AT2 AN.eps_AT2_inf AN.eps_AT2_sup AC.eps_AT2 AC.eps_AT2_inf AC.eps_AT2_sup];

IRF_AT4 = [Y.eps_AT4  Y.eps_AT4_inf  Y.eps_AT4_sup C.eps_AT4  C.eps_AT4_inf C.eps_AT4_sup I.eps_AT4  I.eps_AT4_inf I.eps_AT4_sup tb_y.eps_AT4  tb_y.eps_AT4_inf tb_y.eps_AT4_sup L.eps_AT4  L.eps_AT4_inf L.eps_AT4_sup D.eps_AT4 D.eps_AT4_inf D.eps_AT4_sup reer.eps_AT4 reer.eps_AT4_inf reer.eps_AT4_sup P_C.eps_AT4 P_C.eps_AT4_inf P_C.eps_AT4_sup r.eps_AT4 r.eps_AT4_inf r.eps_AT4_sup AT.eps_AT4 AT.eps_AT4_inf AT.eps_AT4_sup AN.eps_AT4 AN.eps_AT4_inf AN.eps_AT4_sup AC.eps_AT4 AC.eps_AT4_inf AC.eps_AT4_sup];
 

	
  
IRF_AN0 = [Y.eps_AN0  Y.eps_AN0_inf Y.eps_AN0_sup C.eps_AN0  C.eps_AN0_inf C.eps_AN0_sup I.eps_AN0  I.eps_AN0_inf I.eps_AN0_sup tb_y.eps_AN0  tb_y.eps_AN0_inf tb_y.eps_AN0_sup L.eps_AN0  L.eps_AN0_inf L.eps_AN0_sup D.eps_AN0 D.eps_AN0_inf D.eps_AN0_sup reer.eps_AN0 reer.eps_AN0_inf  reer.eps_AN0_sup  P_C.eps_AN0  P_C.eps_AN0_inf P_C.eps_AN0_sup r.eps_AN0 r.eps_AN0_inf r.eps_AN0_sup AT.eps_AN0 AT.eps_AN0_inf AT.eps_AN0_sup AN.eps_AN0 AN.eps_AN0_inf AN.eps_AN0_sup  AC.eps_AN0 AC.eps_AN0_inf AC.eps_AN0_sup];
  
IRF_AN2 = [Y.eps_AN2  Y.eps_AN2_inf Y.eps_AN2_sup C.eps_AN2  C.eps_AN2_inf C.eps_AN2_sup I.eps_AN2  I.eps_AN2_inf I.eps_AN2_sup tb_y.eps_AN2  tb_y.eps_AN2_inf tb_y.eps_AN2_sup	 L.eps_AN2  L.eps_AN2_inf L.eps_AN2_sup D.eps_AN2 D.eps_AN2_inf D.eps_AN2_sup reer.eps_AN2 reer.eps_AN2_inf reer.eps_AN2_sup P_C.eps_AN2 P_C.eps_AN2_inf P_C.eps_AN2_sup r.eps_AN2 r.eps_AN2_inf r.eps_AN2_sup AT.eps_AN2 AT.eps_AN2_inf AT.eps_AN2_sup AN.eps_AN2 AN.eps_AN2_inf AN.eps_AN2_sup AC.eps_AN2 AC.eps_AN2_inf AC.eps_AN2_sup];
  
IRF_AN4 = [Y.eps_AN4  Y.eps_AN4_inf Y.eps_AN4_sup C.eps_AN4  C.eps_AN4_inf C.eps_AN4_sup I.eps_AN4 I.eps_AN4_inf I.eps_AN4_sup tb_y.eps_AN4  tb_y.eps_AN4_inf tb_y.eps_AN4_sup L.eps_AN4  L.eps_AN4_inf L.eps_AN4_sup D.eps_AN4 D.eps_AN4_inf D.eps_AN4_sup reer.eps_AN4 reer.eps_AN4_inf reer.eps_AN4_sup P_C.eps_AN4 P_C.eps_AN4_inf P_C.eps_AN4_sup r.eps_AN4 r.eps_AN4_inf r.eps_AN4_sup AT.eps_AN4 AT.eps_AN4_inf AT.eps_AN4_sup AN.eps_AN4 AN.eps_AN4_inf AN.eps_AN4_sup  AC.eps_AN4 AC.eps_AN4_inf AC.eps_AN4_sup];



IRF_AC0 = [Y.eps_AC0  Y.eps_AC0_inf  Y.eps_AC0_sup  C.eps_AC0  C.eps_AC0_inf  C.eps_AC0_sup  I.eps_AC0  I.eps_AC0_inf  I.eps_AC0_sup tb_y.eps_AC0 tb_y.eps_AC0_inf  tb_y.eps_AC0_sup  L.eps_AC0  L.eps_AC0_inf  L.eps_AC0_sup  D.eps_AC0  D.eps_AC0_inf  D.eps_AC0_sup  reer.eps_AC0  reer.eps_AC0_inf  reer.eps_AC0_sup  P_C.eps_AC0  P_C.eps_AC0_inf P_C.eps_AC0_sup r.eps_AC0  r.eps_AC0_inf  r.eps_AC0_sup  AT.eps_AC0  AT.eps_AC0_inf  AT.eps_AC0_sup  AN.eps_AC0  AN.eps_AC0_inf  AN.eps_AC0_sup  AC.eps_AC0  AC.eps_AC0_inf AC.eps_AC0_sup];
  
IRF_AC2 = [Y.eps_AC2  Y.eps_AC2_inf Y.eps_AC2_sup C.eps_AC2  C.eps_AC2_inf C.eps_AC2_sup I.eps_AC2  I.eps_AC2_inf I.eps_AC2_sup tb_y.eps_AC2  tb_y.eps_AC2_inf tb_y.eps_AC2_sup  L.eps_AC2  L.eps_AC2_inf L.eps_AC2_sup D.eps_AC2  D.eps_AC2_inf  D.eps_AC2_sup  reer.eps_AC2  reer.eps_AC2_inf  reer.eps_AC2_sup  P_C.eps_AC2  P_C.eps_AC2_inf P_C.eps_AC2_sup r.eps_AC2 r.eps_AC2_inf r.eps_AC2_sup AT.eps_AC2 AT.eps_AC2_inf AT.eps_AC2_sup  AN.eps_AC2  AN.eps_AC2_inf AN.eps_AC2_sup AC.eps_AC2 AC.eps_AC2_inf AC.eps_AC2_sup ];
  
IRF_AC4 = [Y.eps_AC4  Y.eps_AC4_inf Y.eps_AC4_sup C.eps_AC4 C.eps_AC4_inf C.eps_AC4_sup I.eps_AC4 I.eps_AC4_inf  I.eps_AC4_sup tb_y.eps_AC4  tb_y.eps_AC4_inf tb_y.eps_AC4_sup L.eps_AC4  L.eps_AC4_inf  L.eps_AC4_sup D.eps_AC4 D.eps_AC4_inf D.eps_AC4_sup reer.eps_AC4  reer.eps_AC4_inf  reer.eps_AC4_sup  P_C.eps_AC4  P_C.eps_AC4_inf P_C.eps_AC4_sup r.eps_AC4 r.eps_AC4_inf r.eps_AC4_sup  AT.eps_AC4 AT.eps_AC4_inf AT.eps_AC4_sup AN.eps_AC4 AN.eps_AC4_inf AN.eps_AC4_sup  AC.eps_AC4 AC.eps_AC4_inf AC.eps_AC4_sup ];
 



% ~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/
filenameCN0 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsCN0_aggregate.csv';
filenameCN2 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsCN2_aggregate.csv';
filenameCN4 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsCN4_aggregate.csv';

filenamer = '/home/cleyton/news_shock_project/results/IRF/IRF_epsr_aggregate.csv';



filenameAT0 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAT0_aggregate.csv';
filenameAT2 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAT2_aggregate.csv';
filenameAT4 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAT4_aggregate.csv';


filenameAN0 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAN0_aggregate.csv';
filenameAN2 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAN2_aggregate.csv';
filenameAN4 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAN4_aggregate.csv';

  
filenameAC0 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAC0_aggregate.csv';
filenameAC2 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAC2_aggregate.csv';
filenameAC4 = '/home/cleyton/news_shock_project/results/IRF/IRF_epsAC4_aggregate.csv';


csvwrite(filenameCN0, IRF_CN0);
csvwrite(filenameCN2, IRF_CN2);
csvwrite(filenameCN4,IRF_CN4);
 
csvwrite(filenamer,IRF_r);

csvwrite(filenameAT0,IRF_AT0);
csvwrite(filenameAT2,IRF_AT2)
csvwrite(filenameAT4,IRF_AT4)

csvwrite(filenameAN0,IRF_AN0);
csvwrite(filenameAN2,IRF_AN2);
csvwrite(filenameAN4,IRF_AN4);

csvwrite(filenameAC0,IRF_AC0);
csvwrite(filenameAC2,IRF_AC2);
csvwrite(filenameAC4,IRF_AC4);



% Forecast Error Variance Decomposition (FEVD):
FEVD_Y = oo_.PosteriorTheoreticalMoments.dsge.ConditionalVarianceDecomposition.Median.Y_hat;
FEVD_C = oo_.PosteriorTheoreticalMoments.dsge.ConditionalVarianceDecomposition.Median.C_hat;
FEVD_I = oo_.PosteriorTheoreticalMoments.dsge.ConditionalVarianceDecomposition.Median.I_hat;
FEVD_L = oo_.PosteriorTheoreticalMoments.dsge.ConditionalVarianceDecomposition.Median.L_hat;
FEVD_TBY = oo_.PosteriorTheoreticalMoments.dsge.ConditionalVarianceDecomposition.Median.tb_y_hat;

save /home/cleyton/news_shock_project/results/FEVD/FEVD_Y_hat.mat FEVD_Y
save /home/cleyton/news_shock_project/results/FEVD/FEVD_C_hat.mat FEVD_C
save /home/cleyton/news_shock_project/results/FEVD/FEVD_I_hat.mat FEVD_I
save /home/cleyton/news_shock_project/results/FEVD/FEVD_L_hat.mat FEVD_L
save /home/cleyton/news_shock_project/results/FEVD/FEVD_TBY_hat.mat FEVD_TBY


% Shock_decomposition:    
% export shock decomposition results:
out = oo_.shock_decomposition;
save /home/cleyton/news_shock_project/results/shock_decomposition/shock_decomposition.mat out

% export endogenous variables names (first dimension):
endo_names = M_.endo_names;
save /home/cleyton/news_shock_project/results/shock_decomposition/endo_names.mat endo_names
    
% export exogenous shock names (second dimension):
exo_names = M_.exo_names;
save /home/cleyton/news_shock_project/results/shock_decomposition/exo_names.mat exo_names
    
}

