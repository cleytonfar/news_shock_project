clear all;
clc;

load intbank31_results;

names1 = char('\epsilon_{z}','\epsilon_{h}','\epsilon_{mi}','\epsilon_{me}','\epsilon_{a}','\epsilon_{l}','\epsilon_{qk}','\epsilon_{y}','\epsilon_{bh}',...
              '\epsilon_{be}','\epsilon_{d}','\epsilon_{r}','\epsilon_{G}','\epsilon_{\delta^{d}}', '\epsilon_{kb}','\epsilon_{ib}');
names3 = char('\rho_{z}','\rho_{h}','\rho_{mi}','\rho_{me}','\rho_{a}','\rho_{l}','\rho_{qk}','\rho_{y}','\rho_{bh}',...
              '\rho_{be}','\rho_{d}','\rho_{G}','\rho_{\delta^{d}}','\rho_{kb}','\rho_{ib}');
names5 =char('k_{p}','k_{bh}','k_{be}','k_{d}','k_{i}','k_{w}','k_{kb}', 'h^{p}','h^{I}','h^{E}','\Theta','\iota_{w}','\iota_{p}','\phi_{R}','\phi_{\pi}','\phi_{y}','\rho^{fp}');



%%%POSTERIOR DENSITY%%%%%%%%%

%% STANDARD DEVIATIONS :
e_C0_posterior  = oo_.posterior_density.shocks_std.eps_C_news0;
e_C2_posterior = oo_.posterior_density.shocks_std.eps_C_news2;
e_C4_posterior = oo_.posterior_density.shocks_std.eps_C_news4;
e_AT0_posterior = oo_.posterior_density.shocks_std.eps_AT_news0;
e_AT2_posterior = oo_.posterior_density.shocks_std.eps_AT_news2;
e_AT4_posterior = oo_.posterior_density.shocks_std.eps_AT_news4;
e_AN0_posterior = oo_.posterior_density.shocks_std.eps_AN_news0;
e_AN2_posterior = oo_.posterior_density.shocks_std.eps_AN_news2;
e_AN4_posterior = oo_.posterior_density.shocks_std.eps_AN_news4;
e_AC0_posterior = oo_.posterior_density.shocks_std.eps_AC_news0;
e_AC2_posterior = oo_.posterior_density.shocks_std.eps_AC_news2;
e_AC4_posterior = oo_.posterior_density.shocks_std.eps_AC_news4;
e_r_posterior = oo_.posterior_density.shocks_std.eps_r;

b_posterior = oo_.posterior_density.parameters.b;
rho_r_posterior = oo_.posterior_density.parameters.rrho_r;
rho_AT_posterior = oo_.posterior_density.parameters.rrho_AT;
rho_AN_posterior = oo_.posterior_density.parameters.rrho_AN;
rho_AC_posterior = oo_.posterior_density.parameters.rrho_AC;
v_D_posterior = oo_.posterior_density.parameters.v_D;
v_C_posterior = oo_.posterior_density.parameters.v_C;
eta_T_posterior = oo_.posterior_density.parameters.eeta_T;
eta_N_posterior = oo_.posterior_density.parameters.eeta_N;
eta_C_posterior = oo_.posterior_density.parameters.eeta_C;
iota_T_posterior = oo_.posterior_density.parameters.iiota_T;
iota_N_posterior = oo_.posterior_density.parameters.iiota_N;
iota_C_posterior = oo_.posterior_density.parameters.iiota_C;



Post1_stderr = [e_C0_posterior(:,2) e_C2_posterior(:,2) e_C4_posterior(:,2) e_AT0_posterior(:,2) e_AT2_posterior(:,2) e_AT4_posterior(:,2) e_AN0_posterior(:,2) e_AN2_posterior(:,2) e_AN4_posterior(:,2) e_AC0_posterior(:,2) e_AC2_posterior(:,2) e_AC4_posterior(:,2) e_r_posterior(:,2)];
Post1_param  = [b_posterior(:,2) rho_r_posterior(:,2) rho_AT_posterior(:,2) rho_AN_posterior(:,2) rho_AC_posterior(:,2) v_D_posterior(:,2) v_C_posterior(:,2) eta_T_posterior(:,2) eta_N_posterior(:,2) eta_C_posterior(:,2) iota_T_posterior(:,2) iota_N_posterior(:,2) iota_C_posterior(:,2)];  

Post1_stderr_support = [e_C0_posterior(:,1) e_C2_posterior(:,1) e_C4_posterior(:,1) e_AT0_posterior(:,1) e_AT2_posterior(:,1) e_AT4_posterior(:,1) e_AN0_posterior(:,1) e_AN2_posterior(:,1) e_AN4_posterior(:,1) e_AC0_posterior(:,1) e_AC2_posterior(:,1) e_AC4_posterior(:,1) e_r_posterior(:,1)];
Post1_param_support  = [b_posterior(:,1) rho_r_posterior(:,1) rho_AT_posterior(:,1) rho_AN_posterior(:,1) rho_AC_posterior(:,1) v_D_posterior(:,1) v_C_posterior(:,1) eta_T_posterior(:,1) eta_N_posterior(:,1) eta_C_posterior(:,1) iota_T_posterior(:,2) iota_N_posterior(:,2) iota_C_posterior(:,2)];  




%% PRIOR 

%% STANDARD DEVIATIONS :
e_C0_prior  = oo_.prior_density.shocks_std.eps_C_news0;
e_C2_prior = oo_.prior_density.shocks_std.eps_C_news2;
e_C4_prior = oo_.prior_density.shocks_std.eps_C_news4;
e_AT0_prior = oo_.prior_density.shocks_std.eps_AT_news0;
e_AT2_prior = oo_.prior_density.shocks_std.eps_AT_news2;
e_AT4_prior = oo_.prior_density.shocks_std.eps_AT_news4;
e_AN0_prior = oo_.prior_density.shocks_std.eps_AN_news0;
e_AN2_prior = oo_.prior_density.shocks_std.eps_AN_news2;
e_AN4_prior = oo_.prior_density.shocks_std.eps_AN_news4;
e_AC0_prior = oo_.prior_density.shocks_std.eps_AC_news0;
e_AC2_prior = oo_.prior_density.shocks_std.eps_AC_news2;
e_AC4_prior = oo_.prior_density.shocks_std.eps_AC_news4;
e_r_prior = oo_.prior_density.shocks_std.eps_r;

b_prior = oo_.prior_density.parameters.b;
rho_r_prior = oo_.prior_density.parameters.rrho_r;
rho_AT_prior = oo_.prior_density.parameters.rrho_AT;
rho_AN_prior = oo_.prior_density.parameters.rrho_AN;
rho_AC_prior = oo_.prior_density.parameters.rrho_AC;
v_D_prior = oo_.prior_density.parameters.v_D;
v_C_prior = oo_.prior_density.parameters.v_C;
eta_T_prior = oo_.prior_density.parameters.eeta_T;
eta_N_prior = oo_.prior_density.parameters.eeta_N;
eta_C_prior = oo_.prior_density.parameters.eeta_C;
iota_T_prior = oo_.prior_density.parameters.iiota_T;
iota_N_prior = oo_.prior_density.parameters.iiota_N;
iota_C_prior = oo_.prior_density.parameters.iiota_C;

Prior1_stderr = [e_C0_prior(:,2) e_C2_prior(:,2)  e_C4_prior(:,2) e_AT0_prior(:,2)  e_AT2_prior(:,2) e_AT4_prior(:,2) e_AN0_prior(:,2) e_AN2_prior(:,2) e_AN4_prior(:,2) e_AC0_prior(:,2) e_AC2_prior(:,2) e_AC4_prior(:,2) e_r_prior(:,2)];
Prior1_param  = [b_prior(:,2) rho_r_prior(:,2) rho_AT_prior(:,2) rho_AN_prior(:,2) rho_AC_prior(:,2) v_D_prior(:,2) v_C_prior(:,2) eta_T_prior(:,2) eta_N_prior(:,2) eta_C_prior(:,2) iota_T_prior(:,2) iota_N_prior(:,2) iota_C(:,2)];  

Prior1_stderr_support = [e_C0_prior(:,1) e_C2_prior(:,1)  e_C4_prior(:,1) e_AT0_prior(:,1)  e_AT2_prior(:,1) e_AT4_prior(:,1) e_AN0_prior(:,1) e_AN2_prior(:,1) e_AN4_prior(:,1) e_AC0_prior(:,1) e_AC2_prior(:,1) e_AC4_prior(:,1) e_r_prior(:,1)];
Prior1_param_support  = [b_prior(:,1) rho_r_prior(:,1) rho_AT_prior(:,1) rho_AN_prior(:,1) rho_AC_prior(:,1) v_D_prior(:,1) v_C_prior(:,1) eta_T_prior(:,1) eta_N_prior(:,1) eta_C_prior(:,1) iota_T_prior(:,1) iota_N_prior(:,1) iota_C(:,1)];  






h=figure('Position', [600, 0, 1000, 900]);
axes ('position', [0, 0, 1, 1]);


figure(1)
for j = 1:16;
    subplot(4,4,j), plot(Post1_stderr_support(:,j),Post1_stderr(:,j),'k', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','b'); hold on;
                    plot(Prior1_stderr_support(:,j),Prior1_stderr(:,j),'-.k', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','r'); hold on;
                    %xlabel(Post1_stderr_support(:,j));
                    %ylabel('% dev from SS');
                    grid on
                    title(names1(j,:),'FontSize',10)
axis tight;                   
end;
legend('Posterior', 'Prior',  'Location', 'Best', 'Orientation', 'horizontal')

% 
% h=figure('Position', [600, 0, 1000, 900]);
% axes ('position', [0, 0, 1, 1]);
% 
% 
% figure(2)
% for j = 1:6;
%     subplot(3,2,j), plot(Post2_stderr_support(:,j),Post2_stderr(:,j),'b', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','b'); hold on;
%                     plot(Prior2_stderr_support(:,j),Prior2_stderr(:,j),'--r', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','b'); hold on;
%                     %xlabel(Post1_stderr_support(:,j));
%                     %ylabel('% dev from SS');
%                     grid on
%                     title(names2(j,:),'FontSize',10)
% axis tight;                   
% end;
% legend('Posterior', 'Prior',  'Location', 'Best', 'Orientation', 'horizontal')
% 
% h=figure('Position', [600, 0, 1000, 900]);
% axes ('position', [0, 0, 1, 1]);
% 
% 
% figure(3)
% for j = 1:10;
%     subplot(5,2,j), plot(Post1_param_support(:,j),Post1_param(:,j),'b', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','b'); hold on;
%                     plot(Prior1_param_support(:,j),Prior1_param(:,j),'--r', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','r'); hold on;
%                     %xlabel(Post1_stderr_support(:,j));
%                     %ylabel('% dev from SS');
%                     grid on
%                     title(names3(j,:),'FontSize',10)
% axis tight;                   
% end;
% 
% legend('Posterior', 'Prior',  'Location', 'Best', 'Orientation', 'horizontal')
% h=figure('Position', [600, 0, 1000, 900]);
% axes ('position', [0, 0, 1, 1]);
% % 
% 
% figure(4)
% for j = 1:4;
%     subplot(2,2,j), plot(Post2_param_support(:,j),Post2_param(:,j),'b', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','b'); hold on;
%                     plot(Prior2_param_support(:,j),Prior2_param(:,j),'--r', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','r'); hold on;
%                     %xlabel(Post1_stderr_support(:,j));
%                     %ylabel('% dev from SS');
%                     grid on
%                     title(names4(j,:),'FontSize',10)
% axis tight;                   
% end;
% legend('Posterior', 'Prior',  'Location', 'Best', 'Orientation', 'horizontal')
% 
% h=figure('Position', [600, 0, 1000, 900]);
% axes ('position', [0, 0, 1, 1]);
% 
% 
% figure(5)
% for j = 1:8;
%     subplot(4,2,j), plot(Post3_param_support(:,j),Post3_param(:,j),'b', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','b'); hold on;
%                     plot(Prior3_param_support(:,j),Prior3_param(:,j),'--r', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','r'); hold on;
%                     %xlabel(Post1_stderr_support(:,j));
%                     %ylabel('% dev from SS');
%                     grid on
%                     title(names5(j,:),'FontSize',10)
% axis tight;                   
% end;
% legend('Posterior', 'Prior',  'Location', 'Best', 'Orientation', 'horizontal')
% 
% h=figure('Position', [600, 0, 1000, 900]);
% axes ('position', [0, 0, 1, 1]);


% figure(1)
% for j = 1:17;
%     subplot(5,4,j), plot(Post3_param_support(:,j),Post3_param(:,j),'k', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','b'); hold on;
%                     plot(Prior3_param_support(:,j),Prior3_param(:,j),'-.k', 'LineWidth',2, 'MarkerSize', 5,'MarkerEdgeColor','k', 'MarkerFaceColor','r'); hold on;
%                  
%                     %xlabel(Post1_stderr_support(:,j));
%                     %ylabel('% dev from SS');
%                     grid on
%                     title(names5(j,:),'FontSize',10)
% axis tight;                   
% end;
% %legend('Posterior', 'Prior',  'Location', 'Best', 'Orientation', 'horizontal')
% 
