%----------------------------------------------------------------------------------
% PROGRAM: GraficosIRF_relative_confidence.m
%
% LANGUAGE: octave/Matlab
% 
% DESCRIPTION: This file extracts IRF's from the results of dynare's estimation
%              and generates plot using octave/matlab functionalities.
% 
% AUTHOR: Marcelo Eduardo 
%
% MODIFIED BY: Cleyton Farias
%
% VERSION: This version rounds the results around 5 decimals. 
% 
% LAST MODIFIED: Feb 14, 2019
% 
%----------------------------------------------------------------------------------


%close all
%dynare master.mod; 


{

% Commodity Price Shock: 
P_C.eps_CN0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_C_news0;
r.eps_CN0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_C_news0;
Y.eps_CN0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_C_news0;
C.eps_CN0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_C_news0;
I.eps_CN0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_C_news0;
tb_y.eps_CN0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_C_news0;
L.eps_CN0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_C_news0;
D.eps_CN0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_C_news0;
AT.eps_CN0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_C_news0;
AN.eps_CN0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_C_news0;
AC.eps_CN0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_C_news0;

P_C.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_C_news0;
r.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_C_news0;
Y.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_C_news0;
C.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_C_news0;
I.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_C_news0;
tb_y.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_C_news0;
L.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_C_news0;
D.eps_CN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_C_news0;
AT.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_C_news0;
AN.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_C_news0;
AC.eps_CN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_C_news0;

P_C.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_C_news0;
r.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_C_news0;
Y.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_C_news0;
C.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_C_news0;
I.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_C_news0;
tb_y.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_C_news0;
L.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_C_news0;
D.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_C_news0;
AT.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_C_news0;
AN.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_C_news0;
AC.eps_CN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_C_news0;



% News Shocks 2 periods in advance: 
P_C.eps_CN2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_C_news2;
r.eps_CN2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_C_news2;
Y.eps_CN2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_C_news2;
C.eps_CN2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_C_news2;
I.eps_CN2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_C_news2;
tb_y.eps_CN2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_C_news2;
L.eps_CN2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_C_news2;
D.eps_CN2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_C_news2;
AT.eps_CN2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_C_news2;
AN.eps_CN2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_C_news2;
AC.eps_CN2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_C_news2;

P_C.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_C_news2;
r.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_C_news2;
Y.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_C_news2;
C.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_C_news2;
I.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_C_news2;
tb_y.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_C_news2;
L.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_C_news2;
D.eps_CN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_C_news2;
AT.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_C_news2;
AN.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_C_news2;
AC.eps_CN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_C_news2;

P_C.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_C_news2;
r.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_C_news2;
Y.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_C_news2;
C.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_C_news2;
I.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_C_news2;
tb_y.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_C_news2;
L.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_C_news2;
D.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_C_news2;
AT.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_C_news2;
AN.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_C_news2;
AC.eps_CN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_C_news2;

 
% News Shocks 4 periods in advance: 
P_C.eps_CN4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_C_news4;
r.eps_CN4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_C_news4;
Y.eps_CN4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_C_news4;
C.eps_CN4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_C_news4;
I.eps_CN4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_C_news4;
tb_y.eps_CN4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_C_news4;
L.eps_CN4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_C_news4;
D.eps_CN4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_C_news4;
AT.eps_CN4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_C_news4;
AN.eps_CN4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_C_news4;
AC.eps_CN4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_C_news4;

P_C.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_C_news4;
r.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_C_news4;
Y.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_C_news4;
C.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_C_news4;
I.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_C_news4;
tb_y.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_C_news4;
L.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_C_news4;
D.eps_CN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_C_news4;
AT.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_C_news4;
AN.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_C_news4;
AC.eps_CN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_C_news4;

P_C.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_C_news4;
r.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_C_news4;
Y.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_C_news4;
C.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_C_news4;
I.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_C_news4;
tb_y.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_C_news4;
L.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_C_news4;
D.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_C_news4;
AT.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_C_news4;
AN.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_C_news4;
AC.eps_CN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_C_news4;




 % Interest rate shock: 
P_C.eps_r = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_r;
r.eps_r  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_r;
Y.eps_r = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_r;
C.eps_r = oo_.PosteriorIRF.dsge.Median.C_hat_eps_r;
I.eps_r = oo_.PosteriorIRF.dsge.Median.I_hat_eps_r;
tb_y.eps_r = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_r;
L.eps_r = oo_.PosteriorIRF.dsge.Median.L_hat_eps_r;
D.eps_r = oo_.PosteriorIRF.dsge.Median.D_hat_eps_r;
AT.eps_r = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_r;
AN.eps_r = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_r;
AC.eps_r = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_r;

P_C.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_r;
r.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_r;
Y.eps_r_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_r;
C.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_r;
I.eps_r_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_r;
tb_y.eps_r_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_r;
L.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_r;
D.eps_r_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_r;
AT.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_r;
AN.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_r;
AC.eps_r_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_r;

P_C.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_r;
r.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_r;
Y.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_r;
C.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_r;
I.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_r;
tb_y.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_r;
L.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_r;
D.eps_r_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_r;
AT.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_r;
AN.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_r;
AC.eps_r_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_r;


 

% Shock productivity Tradable sector: 
%P_C.eps_AT = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AT;
%r.eps_AT  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AT;
%Y.eps_AT = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AT;
%C.eps_AT = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AT;
%I.eps_AT = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AT;
%tb_y.eps_AT = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AT;
%L.eps_AT = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AT;
%D.eps_AT = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AT;
%AT.eps_AT = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AT;
%AN.eps_AT = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AT;
%AC.eps_AT = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AT;
 
%P_C.eps_AT_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AT;
%r.eps_AT_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AT;
%Y.eps_AT_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AT;
%C.eps_AT_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AT;
%I.eps_AT_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AT;
%tb_y.eps_AT_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AT;
%L.eps_AT_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AT;
%D.eps_AT_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AT;
%AT.eps_AT_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AT;
%AN.eps_AT_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AT;
%AC.eps_AT_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AT;

%P_C.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AT;
%r.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AT;
%Y.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AT;
%C.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AT;
%I.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AT;
%tb_y.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AT;
%L.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AT;
%D.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AT;
%AT.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AT;
%AN.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AT;
%AC.eps_AT_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AT;



 
% unanticipated Shock productivity Tradable sector: 
P_C.eps_AT0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AT_news0;
r.eps_AT0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AT_news0;
Y.eps_AT0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AT_news0;
C.eps_AT0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AT_news0;
I.eps_AT0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AT_news0;
tb_y.eps_AT0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AT_news0;
L.eps_AT0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AT_news0;
D.eps_AT0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AT_news0;
AT.eps_AT0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AT_news0;
AN.eps_AT0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AT_news0;
AC.eps_AT0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AT_news0;
 
P_C.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AT_news0;
r.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AT_news0;
Y.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AT_news0;
C.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AT_news0;
I.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AT_news0;
tb_y.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AT_news0;
L.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AT_news0;
D.eps_AT0_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AT_news0;
AT.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AT_news0;
AN.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AT_news0;
AC.eps_AT0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AT_news0;

P_C.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AT_news0;
r.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AT_news0;
Y.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AT_news0;
C.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AT_news0;
I.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AT_news0;
tb_y.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AT_news0;
L.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AT_news0;
D.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AT_news0;
AT.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AT_news0;
AN.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AT_news0;
AC.eps_AT0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AT_news0;



 
% News with 2 periods in advance productivity Tradable sector: 
P_C.eps_AT2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AT_news2;
r.eps_AT2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AT_news2;
Y.eps_AT2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AT_news2;
C.eps_AT2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AT_news2;
I.eps_AT2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AT_news2;
tb_y.eps_AT2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AT_news2;
L.eps_AT2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AT_news2;
D.eps_AT2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AT_news2;
AT.eps_AT2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AT_news2;
AN.eps_AT2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AT_news2;
AC.eps_AT2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AT_news2;
 
P_C.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AT_news2;
r.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AT_news2;
Y.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AT_news2;
C.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AT_news2;
I.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AT_news2;
tb_y.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AT_news2;
L.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AT_news2;
D.eps_AT2_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AT_news2;
AT.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AT_news2;
AN.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AT_news2;
AC.eps_AT2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AT_news2;

P_C.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AT_news2;
r.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AT_news2;
Y.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AT_news2;
C.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AT_news2;
I.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AT_news2;
tb_y.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AT_news2;
L.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AT_news2;
D.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AT_news2;
AT.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AT_news2;
AN.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AT_news2;
AC.eps_AT2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AT_news2;



% News with 4 periods in advance productivity Tradable sector: 
P_C.eps_AT4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AT_news4;
r.eps_AT4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AT_news4;
Y.eps_AT4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AT_news4;
C.eps_AT4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AT_news4;
I.eps_AT4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AT_news4;
tb_y.eps_AT4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AT_news4;
L.eps_AT4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AT_news4;
D.eps_AT4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AT_news4;
AT.eps_AT4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AT_news4;
AN.eps_AT4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AT_news4;
AC.eps_AT4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AT_news4;
 
P_C.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AT_news4;
r.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AT_news4;
Y.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AT_news4;
C.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AT_news4;
I.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AT_news4;
tb_y.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AT_news4;
L.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AT_news4;
D.eps_AT4_inf =  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AT_news4;
AT.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AT_news4;
AN.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AT_news4;
AC.eps_AT4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AT_news4;

P_C.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AT_news4;
r.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AT_news4;
Y.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AT_news4;
C.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AT_news4;
I.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AT_news4;
tb_y.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AT_news4;
L.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AT_news4;
D.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AT_news4;
AT.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AT_news4;
AN.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AT_news4;
AC.eps_AT4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AT_news4;




 

% Shock productivity NonTradable sector: 
%P_C.eps_AN = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AN;
%r.eps_AN  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AN;
%Y.eps_AN = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AN;
%C.eps_AN = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AN;
%I.eps_AN = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AN;
%tb_y.eps_AN = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AN;
%L.eps_AN = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AN;
%D.eps_AN = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AN;
%AT.eps_AN = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AN;
%AN.eps_AN = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AN;
%AC.eps_AN = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AN;

%P_C.eps_AN_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AN;
%r.eps_AN_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AN;
%Y.eps_AN_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AN;
%C.eps_AN_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AN;
%I.eps_AN_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AN;
%tb_y.eps_AN_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AN;
%L.eps_AN_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AN;
%D.eps_AN_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AN;
%AT.eps_AN_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AN;
%AN.eps_AN_inf  = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AN;
%AC.eps_AN_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AN;

%P_C.eps_AN_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AN;
%r.eps_AN_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AN;
%Y.eps_AN_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AN;
%C.eps_AN_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AN;
%I.eps_AN_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AN;
%tb_y.eps_AN_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AN;
%L.eps_AN_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AN;
%D.eps_AN_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AN;
%AT.eps_AN_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AN;
%AN.eps_AN_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AN;
%AC.eps_AN_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AN;


 

% Shock productivity NonTradable sector: 
P_C.eps_AN0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AN_news0;
r.eps_AN0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AN_news0;
Y.eps_AN0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AN_news0;
C.eps_AN0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AN_news0;
I.eps_AN0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AN_news0;
tb_y.eps_AN0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AN_news0;
L.eps_AN0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AN_news0;
D.eps_AN0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AN_news0;
AT.eps_AN0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AN_news0;
AN.eps_AN0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AN_news0;
AC.eps_AN0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AN_news0;

P_C.eps_AN0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AN_news0;
r.eps_AN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AN_news0;
Y.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AN_news0;
C.eps_AN0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AN_news0;
I.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AN_news0;
tb_y.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AN_news0;
L.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AN_news0;
D.eps_AN0_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AN_news0;
AT.eps_AN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AN_news0;
AN.eps_AN0_inf  = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AN_news0;
AC.eps_AN0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AN_news0;

P_C.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AN_news0;
r.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AN_news0;
Y.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AN_news0;
C.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AN_news0;
I.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AN_news0;
tb_y.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AN_news0;
L.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AN_news0;
D.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AN_news0;
AT.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AN_news0;
AN.eps_AN0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AN_news0;
AC.eps_AN0_sup= oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AN_news0;


% News with 2 periods in advance productivity NonTradable sector: 
P_C.eps_AN2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AN_news2;
r.eps_AN2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AN_news2;
Y.eps_AN2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AN_news2;
C.eps_AN2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AN_news2;
I.eps_AN2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AN_news2;
tb_y.eps_AN2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AN_news2;
L.eps_AN2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AN_news2;
D.eps_AN2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AN_news2;
AT.eps_AN2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AN_news2;
AN.eps_AN2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AN_news2;
AC.eps_AN2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AN_news2;

P_C.eps_AN2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AN_news2;
r.eps_AN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AN_news2;
Y.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AN_news2;
C.eps_AN2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AN_news2;
I.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AN_news2;
tb_y.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AN_news2;
L.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AN_news2;
D.eps_AN2_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AN_news2;
AT.eps_AN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AN_news2;
AN.eps_AN2_inf  = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AN_news2;
AC.eps_AN2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AN_news2;

P_C.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AN_news2;
r.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AN_news2;
Y.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AN_news2;
C.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AN_news2;
I.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AN_news2;
tb_y.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AN_news2;
L.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AN_news2;
D.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AN_news2;
AT.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AN_news2;
AN.eps_AN2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AN_news2;
AC.eps_AN2_sup= oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AN_news2;


% News with 4 periods in advance productivity NonTradable sector: 
P_C.eps_AN4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AN_news4;
r.eps_AN4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AN_news4;
Y.eps_AN4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AN_news4;
C.eps_AN4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AN_news4;
I.eps_AN4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AN_news4;
tb_y.eps_AN4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AN_news4;
L.eps_AN4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AN_news4;
D.eps_AN4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AN_news4;
AT.eps_AN4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AN_news4;
AN.eps_AN4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AN_news4;
AC.eps_AN4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AN_news4;

P_C.eps_AN4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AN_news4;
r.eps_AN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AN_news4;
Y.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AN_news4;
C.eps_AN4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AN_news4;
I.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AN_news4;
tb_y.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AN_news4;
L.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AN_news4;
D.eps_AN4_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AN_news4;
AT.eps_AN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AN_news4;
AN.eps_AN4_inf  = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AN_news4;
AC.eps_AN4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AN_news4;

P_C.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AN_news4;
r.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AN_news4;
Y.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AN_news4;
C.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AN_news4;
I.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AN_news4;
tb_y.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AN_news4;
L.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AN_news4;
D.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AN_news4;
AT.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AN_news4;
AN.eps_AN4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AN_news4;
AC.eps_AN4_sup= oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AN_news4;




 
% Shock productivity Commodity sector: 
%P_C.eps_AC = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AC;
%r.eps_AC  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AC;
%Y.eps_AC = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AC;
%C.eps_AC = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AC;
%I.eps_AC = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AC;
%tb_y.eps_AC = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AC;
%L.eps_AC = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AC;
%D.eps_AC = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AC;
%AT.eps_AC = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AC;
%AN.eps_AC = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AC;
%AC.eps_AC = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AC;
 
%P_C.eps_AC_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AC;
%r.eps_AC_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AC;
%Y.eps_AC_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AC;
%C.eps_AC_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AC;
%I.eps_AC_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AC;
%tb_y.eps_AC_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AC;
%L.eps_AC_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AC;
%D.eps_AC_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AC;
%AT.eps_AC_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AC;
%AN.eps_AC_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AC;
%AC.eps_AC_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AC;
 
%P_C.eps_AC_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AC;
%r.eps_AC_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AC;
%Y.eps_AC_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AC;
%C.eps_AC_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AC;
%I.eps_AC_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AC;
%tb_y.eps_AC_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AC;
%L.eps_AC_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AC;
%D.eps_AC_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AC;
%AT.eps_AC_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AC;
%AN.eps_AC_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AC;
%AC.eps_AC_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AC;
 


 
% Shock productivity Commodity sector: 
P_C.eps_AC0 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AC_news0;
r.eps_AC0  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AC_news0;
Y.eps_AC0 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AC_news0;
C.eps_AC0 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AC_news0;
I.eps_AC0 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AC_news0;
tb_y.eps_AC0 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AC_news0;
L.eps_AC0 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AC_news0;
D.eps_AC0 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AC_news0;
AT.eps_AC0 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AC_news0;
AN.eps_AC0 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AC_news0;
AC.eps_AC0 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AC_news0;
 
P_C.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AC_news0;
r.eps_AC0_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AC_news0;
Y.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AC_news0;
C.eps_AC0_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AC_news0;
I.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AC_news0;
tb_y.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AC_news0;
L.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AC_news0;
D.eps_AC0_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AC_news0;
AT.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AC_news0;
AN.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AC_news0;
AC.eps_AC0_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AC_news0;
 
P_C.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AC_news0;
r.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AC_news0;
Y.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AC_news0;
C.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AC_news0;
I.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AC_news0;
tb_y.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AC_news0;
L.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AC_news0;
D.eps_AC0_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AC_news0;
AT.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AC_news0;
AN.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AC_news0;
AC.eps_AC0_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AC_news0;


% News Shock productivity Commodity sector with 2 periods in advance: 
P_C.eps_AC2 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AC_news2;
r.eps_AC2  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AC_news2;
Y.eps_AC2 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AC_news2;
C.eps_AC2 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AC_news2;
I.eps_AC2 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AC_news2;
tb_y.eps_AC2 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AC_news2;
L.eps_AC2 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AC_news2;
D.eps_AC2 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AC_news2;
AT.eps_AC2 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AC_news2;
AN.eps_AC2 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AC_news2;
AC.eps_AC2 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AC_news2;
 
P_C.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AC_news2;
r.eps_AC2_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AC_news2;
Y.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AC_news2;
C.eps_AC2_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AC_news2;
I.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AC_news2;
tb_y.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AC_news2;
L.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AC_news2;
D.eps_AC2_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AC_news2;
AT.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AC_news2;
AN.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AC_news2;
AC.eps_AC2_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AC_news2;
 
P_C.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AC_news2;
r.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AC_news2;
Y.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AC_news2;
C.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AC_news2;
I.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AC_news2;
tb_y.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AC_news2;
L.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AC_news2;
D.eps_AC2_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AC_news2;
AT.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AC_news2;
AN.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AC_news2;
AC.eps_AC2_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AC_news2;
 

% News Shock productivity Commodity sector with 4 periods in advance: 
P_C.eps_AC4 = oo_.PosteriorIRF.dsge.Median.P_C_hat_eps_AC_news4;
r.eps_AC4  = oo_.PosteriorIRF.dsge.Median.r_hat_eps_AC_news4;
Y.eps_AC4 = oo_.PosteriorIRF.dsge.Median.Y_hat_eps_AC_news4;
C.eps_AC4 = oo_.PosteriorIRF.dsge.Median.C_hat_eps_AC_news4;
I.eps_AC4 = oo_.PosteriorIRF.dsge.Median.I_hat_eps_AC_news4;
tb_y.eps_AC4 = oo_.PosteriorIRF.dsge.Median.tb_y_hat_eps_AC_news4;
L.eps_AC4 = oo_.PosteriorIRF.dsge.Median.L_hat_eps_AC_news4;
D.eps_AC4 = oo_.PosteriorIRF.dsge.Median.D_hat_eps_AC_news4;
AT.eps_AC4 = oo_.PosteriorIRF.dsge.Median.A_T_hat_eps_AC_news4;
AN.eps_AC4 = oo_.PosteriorIRF.dsge.Median.A_N_hat_eps_AC_news4;
AC.eps_AC4 = oo_.PosteriorIRF.dsge.Median.A_C_hat_eps_AC_news4;
 
P_C.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.P_C_hat_eps_AC_news4;
r.eps_AC4_inf =  oo_.PosteriorIRF.dsge.HPDinf.r_hat_eps_AC_news4;
Y.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.Y_hat_eps_AC_news4;
C.eps_AC4_inf =  oo_.PosteriorIRF.dsge.HPDinf.C_hat_eps_AC_news4;
I.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.I_hat_eps_AC_news4;
tb_y.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.tb_y_hat_eps_AC_news4;
L.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.L_hat_eps_AC_news4;
D.eps_AC4_inf=  oo_.PosteriorIRF.dsge.HPDinf.D_hat_eps_AC_news4;
AT.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_T_hat_eps_AC_news4;
 AN.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_N_hat_eps_AC_news4;
AC.eps_AC4_inf = oo_.PosteriorIRF.dsge.HPDinf.A_C_hat_eps_AC_news4;
 
P_C.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.P_C_hat_eps_AC_news4;
r.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.r_hat_eps_AC_news4;
Y.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.Y_hat_eps_AC_news4;
C.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.C_hat_eps_AC_news4;
I.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.I_hat_eps_AC_news4;
tb_y.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.tb_y_hat_eps_AC_news4;
L.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.L_hat_eps_AC_news4;
D.eps_AC4_sup= oo_.PosteriorIRF.dsge.HPDsup.D_hat_eps_AC_news4;
AT.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_T_hat_eps_AC_news4;
AN.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_N_hat_eps_AC_news4;
AC.eps_AC4_sup = oo_.PosteriorIRF.dsge.HPDsup.A_C_hat_eps_AC_news4;
 




IRF_CN0 = [Y.eps_CN0  Y.eps_CN0_inf Y.eps_CN0_sup C.eps_CN0 C.eps_CN0_inf C.eps_CN0_sup I.eps_CN0  I.eps_CN0_inf I.eps_CN0_sup  tb_y.eps_CN0  tb_y.eps_CN0_inf tb_y.eps_CN0_sup	 L.eps_CN0  L.eps_CN0_inf L.eps_CN0_sup	 D.eps_CN0 D.eps_CN0_inf D.eps_CN0_sup P_C.eps_CN0 P_C.eps_CN0_inf P_C.eps_CN0_sup r.eps_CN0 r.eps_CN0_inf r.eps_CN0_sup AT.eps_CN0 AT.eps_CN0_inf AT.eps_CN0_sup AN.eps_CN0 AN.eps_CN0_inf AN.eps_CN0_sup AC.eps_CN0 AC.eps_CN0_inf AC.eps_CN0_sup];

IRF_CN2 = [Y.eps_CN2  Y.eps_CN2_inf Y.eps_CN2_sup C.eps_CN2 C.eps_CN2_inf C.eps_CN2_sup I.eps_CN2  I.eps_CN2_inf I.eps_CN2_sup tb_y.eps_CN2  tb_y.eps_CN2_inf tb_y.eps_CN2_sup L.eps_CN2  L.eps_CN2_inf L.eps_CN2_sup D.eps_CN2 D.eps_CN2_inf D.eps_CN2_sup	 P_C.eps_CN2 P_C.eps_CN2_inf P_C.eps_CN2_sup r.eps_CN2 r.eps_CN2_inf r.eps_CN2_sup AT.eps_CN2 AT.eps_CN2_inf AT.eps_CN2_sup AN.eps_CN2 AN.eps_CN2_inf AN.eps_CN2_sup AC.eps_CN2 AC.eps_CN2_inf AC.eps_CN2_sup];
 
IRF_CN4 = [Y.eps_CN4  Y.eps_CN4_inf Y.eps_CN4_sup C.eps_CN4 C.eps_CN4_inf C.eps_CN4_sup I.eps_CN4  I.eps_CN4_inf I.eps_CN4_sup tb_y.eps_CN4  tb_y.eps_CN4_inf tb_y.eps_CN4_sup L.eps_CN4  L.eps_CN4_inf L.eps_CN4_sup D.eps_CN4 D.eps_CN4_inf D.eps_CN4_sup P_C.eps_CN4 P_C.eps_CN4_inf P_C.eps_CN4_sup r.eps_CN4 r.eps_CN4_inf r.eps_CN4_sup AT.eps_CN4 AT.eps_CN4_inf AT.eps_CN4_sup AN.eps_CN4 AN.eps_CN4_inf AN.eps_CN4_sup AC.eps_CN4 AC.eps_CN4_inf AC.eps_CN4_sup];


 
IRF_r = [Y.eps_r  Y.eps_r_inf Y.eps_r_sup C.eps_r  C.eps_r_inf C.eps_r_sup I.eps_r  I.eps_r_inf I.eps_r_sup tb_y.eps_r  tb_y.eps_r_inf tb_y.eps_r_sup L.eps_r  L.eps_r_inf L.eps_r_sup D.eps_r D.eps_r_inf D.eps_r_sup P_C.eps_r P_C.eps_r_inf P_C.eps_r_sup r.eps_r r.eps_r_inf r.eps_r_sup AT.eps_r AT.eps_r_inf AT.eps_r_sup AN.eps_r AN.eps_r_inf AN.eps_r_sup AC.eps_r AC.eps_r_inf AC.eps_r_sup];
 

 
% IRF_AT = [Y.eps_AT  Y.eps_AT_inf Y.eps_AT_sup C.eps_AT  C.eps_AT_inf C.eps_AT_sup I.eps_AT  I.eps_AT_inf I.eps_AT_sup tb_y.eps_AT  tb_y.eps_AT_inf tb_y.eps_AT_sup	L.eps_AT  L.eps_AT_inf L.eps_AT_sup	D.eps_AT D.eps_AT_inf D.eps_AT_sup P_C.eps_AT P_C.eps_AT_inf P_C.eps_AT_sup	r.eps_AT r.eps_AT_inf r.eps_AT_sup	AT.eps_AT AT.eps_AT_inf AT.eps_AT_sup AT.eps_AT AT.eps_AT_inf AT.eps_AT_sup	AN.eps_AT AN.eps_AT_inf AN.eps_AT_sup AC.eps_AT AC.eps_AT_inf AC.eps_AT_sup];

IRF_AT0 = [Y.eps_AT0  Y.eps_AT0_inf Y.eps_AT0_sup C.eps_AT0  C.eps_AT0_inf C.eps_AT0_sup I.eps_AT0  I.eps_AT0_inf I.eps_AT0_sup tb_y.eps_AT0  tb_y.eps_AT0_inf tb_y.eps_AT0_sup	L.eps_AT0  L.eps_AT0_inf L.eps_AT0_sup	D.eps_AT0 D.eps_AT0_inf D.eps_AT0_sup P_C.eps_AT0 P_C.eps_AT0_inf P_C.eps_AT0_sup	r.eps_AT0 r.eps_AT0_inf r.eps_AT0_sup	AT.eps_AT0 AT.eps_AT0_inf AT.eps_AT0_sup AN.eps_AT0 AN.eps_AT0_inf AN.eps_AT0_sup AC.eps_AT0 AC.eps_AT0_inf AC.eps_AT0_sup];
  
IRF_AT2 = [Y.eps_AT2  Y.eps_AT2_inf Y.eps_AT2_sup C.eps_AT2  C.eps_AT2_inf C.eps_AT2_sup I.eps_AT2  I.eps_AT2_inf I.eps_AT2_sup tb_y.eps_AT2  tb_y.eps_AT2_inf tb_y.eps_AT2_sup	L.eps_AT2  L.eps_AT2_inf L.eps_AT2_sup D.eps_AT2 D.eps_AT2_inf D.eps_AT2_sup P_C.eps_AT2 P_C.eps_AT2_inf P_C.eps_AT2_sup r.eps_AT2 r.eps_AT2_inf r.eps_AT2_sup	AT.eps_AT2 AT.eps_AT2_inf AT.eps_AT2_sup AN.eps_AT2 AN.eps_AT2_inf AN.eps_AT2_sup AC.eps_AT2 AC.eps_AT2_inf AC.eps_AT2_sup];

IRF_AT4 = [Y.eps_AT4  Y.eps_AT4_inf Y.eps_AT4_sup C.eps_AT4  C.eps_AT4_inf C.eps_AT4_sup I.eps_AT4  I.eps_AT4_inf I.eps_AT4_sup tb_y.eps_AT4  tb_y.eps_AT4_inf tb_y.eps_AT4_sup	L.eps_AT4  L.eps_AT4_inf L.eps_AT4_sup D.eps_AT4 D.eps_AT4_inf D.eps_AT4_sup P_C.eps_AT4 P_C.eps_AT4_inf P_C.eps_AT4_sup r.eps_AT4 r.eps_AT4_inf r.eps_AT4_sup	AT.eps_AT4 AT.eps_AT4_inf AT.eps_AT4_sup AN.eps_AT4 AN.eps_AT4_inf AN.eps_AT4_sup AC.eps_AT4 AC.eps_AT4_inf AC.eps_AT4_sup];
 

	
%IRF_AN = [Y.eps_AN  Y.eps_AN_inf Y.eps_AN_sup C.eps_AN  C.eps_AN_inf C.eps_AN_sup I.eps_AN  I.eps_AN_inf I.eps_AN_sup tb_y.eps_AN  tb_y.eps_AN_inf tb_y.eps_AN_sup	L.eps_AN  L.eps_AN_inf L.eps_AN_sup	D.eps_AN D.eps_AN_inf D.eps_AN_sup	P_C.eps_AN P_C.eps_AN_inf P_C.eps_AN_sup r.eps_AN r.eps_AN_inf r.eps_AN_sup	AT.eps_AN AT.eps_AN_inf AT.eps_AN_sup AN.eps_AN AN.eps_AN_inf AN.eps_AN_sup	AC.eps_AN AC.eps_AN_inf AC.eps_AN_sup];
  
IRF_AN0 = [Y.eps_AN0  Y.eps_AN0_inf Y.eps_AN0_sup C.eps_AN0  C.eps_AN0_inf C.eps_AN0_sup I.eps_AN0  I.eps_AN0_inf I.eps_AN0_sup tb_y.eps_AN0  tb_y.eps_AN0_inf tb_y.eps_AN0_sup	L.eps_AN0  L.eps_AN0_inf L.eps_AN0_sup	D.eps_AN0 D.eps_AN0_inf D.eps_AN0_sup P_C.eps_AN0 P_C.eps_AN0_inf P_C.eps_AN0_sup r.eps_AN0 r.eps_AN0_inf r.eps_AN0_sup AT.eps_AN0 AT.eps_AN0_inf AT.eps_AN0_sup AN.eps_AN0 AN.eps_AN0_inf AN.eps_AN0_sup  AC.eps_AN0 AC.eps_AN0_inf AC.eps_AN0_sup];
  
IRF_AN2 = [Y.eps_AN2  Y.eps_AN2_inf Y.eps_AN2_sup C.eps_AN2  C.eps_AN2_inf C.eps_AN2_sup I.eps_AN2  I.eps_AN2_inf I.eps_AN2_sup tb_y.eps_AN2  tb_y.eps_AN2_inf tb_y.eps_AN2_sup	L.eps_AN2  L.eps_AN2_inf L.eps_AN2_sup D.eps_AN2 D.eps_AN2_inf D.eps_AN2_sup P_C.eps_AN2 P_C.eps_AN2_inf P_C.eps_AN2_sup r.eps_AN2 r.eps_AN2_inf r.eps_AN2_sup AT.eps_AN2 AT.eps_AN2_inf AT.eps_AN2_sup AN.eps_AN2 AN.eps_AN2_inf AN.eps_AN2_sup AC.eps_AN2 AC.eps_AN2_inf AC.eps_AN2_sup];
  
IRF_AN4 = [Y.eps_AN4  Y.eps_AN4_inf Y.eps_AN4_sup C.eps_AN4  C.eps_AN4_inf C.eps_AN4_sup I.eps_AN4 I.eps_AN4_inf I.eps_AN4_sup tb_y.eps_AN4  tb_y.eps_AN4_inf tb_y.eps_AN4_sup	L.eps_AN4  L.eps_AN4_inf L.eps_AN4_sup D.eps_AN4 D.eps_AN4_inf D.eps_AN4_sup P_C.eps_AN4 P_C.eps_AN4_inf P_C.eps_AN4_sup r.eps_AN4 r.eps_AN4_inf r.eps_AN4_sup	AT.eps_AN4 AT.eps_AN4_inf AT.eps_AN4_sup AN.eps_AN4 AN.eps_AN4_inf AN.eps_AN4_sup  AC.eps_AN4 AC.eps_AN4_inf AC.eps_AN4_sup];
					

 
%IRF_AC = [Y.eps_AC  Y.eps_AC_inf Y.eps_AC_sup	C.eps_AC  C.eps_AC_inf C.eps_AC_sup	I.eps_AC  I.eps_AC_inf I.eps_AC_sup	tb_y.eps_AC  tb_y.eps_AC_inf tb_y.eps_AC_sup L.eps_AC  L.eps_AC_inf L.eps_AC_sup	D.eps_AC D.eps_AC_inf D.eps_AC_sup P_C.eps_AC P_C.eps_AC_inf P_C.eps_AC_sup	r.eps_AC r.eps_AC_inf r.eps_AC_sup	AT.eps_AC AT.eps_AC_inf AT.eps_AC_sup AN.eps_AC AN.eps_AC_inf AN.eps_AC_sup	AC.eps_AC AC.eps_AC_inf AC.eps_AC_sup ];

 
IRF_AC0 = [Y.eps_AC0  Y.eps_AC0_inf Y.eps_AC_sup  C.eps_AC0  C.eps_AC0_inf C.eps_AC0_sup  I.eps_AC0 I.eps_AC0_inf I.eps_AC0_sup	tb_y.eps_AC0 tb_y.eps_AC0_inf tb_y.eps_AC0_sup L.eps_AC0 L.eps_AC0_inf  L.eps_AC0_sup D.eps_AC0 D.eps_AC0_inf D.eps_AC0_sup P_C.eps_AC0 P_C.eps_AC0_inf P_C.eps_AC0_sup	r.eps_AC0 r.eps_AC0_inf r.eps_AC0_sup  AT.eps_AC0 AT.eps_AC0_inf AT.eps_AC0_sup AN.eps_AC0 AN.eps_AC0_inf AN.eps_AC0_sup AC.eps_AC0 AC.eps_AC0_inf AC.eps_AC0_sup ];
  
IRF_AC2 = [Y.eps_AC2  Y.eps_AC2_inf Y.eps_AC2_sup C.eps_AC2  C.eps_AC2_inf C.eps_AC2_sup I.eps_AC2  I.eps_AC2_inf I.eps_AC2_sup	tb_y.eps_AC2  tb_y.eps_AC2_inf tb_y.eps_AC2_sup  L.eps_AC2  L.eps_AC2_inf L.eps_AC2_sup	D.eps_AC2 D.eps_AC2_inf D.eps_AC2_sup P_C.eps_AC2  P_C.eps_AC2_inf P_C.eps_AC2_sup r.eps_AC2 r.eps_AC2_inf r.eps_AC2_sup AT.eps_AC2 AT.eps_AC2_inf AT.eps_AC2_sup AN.eps_AC2 AN.eps_AC2_inf AN.eps_AC2_sup	AC.eps_AC2 AC.eps_AC2_inf AC.eps_AC2_sup ];
  
IRF_AC4 = [Y.eps_AC4  Y.eps_AC4_inf Y.eps_AC4_sup C.eps_AC4 C.eps_AC4_inf C.eps_AC4_sup	I.eps_AC4 I.eps_AC4_inf  I.eps_AC4_sup	tb_y.eps_AC4  tb_y.eps_AC4_inf tb_y.eps_AC4_sup L.eps_AC4  L.eps_AC4_inf  L.eps_AC4_sup	D.eps_AC4 D.eps_AC4_inf D.eps_AC4_sup P_C.eps_AC4  P_C.eps_AC4_inf P_C.eps_AC4_sup	r.eps_AC4 r.eps_AC4_inf r.eps_AC4_sup  AT.eps_AC4 AT.eps_AC4_inf AT.eps_AC4_sup AN.eps_AC4 AN.eps_AC4_inf AN.eps_AC4_sup  AC.eps_AC4 AC.eps_AC4_inf AC.eps_AC4_sup ];
 
					

% ~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/
filenameCN0 = '~/brazil/irf_dsge/IRF_epsCN0_aggregate.csv';
filenameCN2 = '~/brazil/irf_dsge/IRF_epsCN2_aggregate.csv';
filenameCN4 = '~/brazil/irf_dsge/IRF_epsCN4_aggregate.csv';

filenamer = '~/brazil/irf_dsge/IRF_epsr_aggregate.csv';

%filenameAT = '~/brazil/irf_dsge/IRF_epsAT_aggregate.csv';

filenameAT0 = '~/brazil/irf_dsge/IRF_epsAT0_aggregate.csv';
filenameAT2 = '~/brazil/irf_dsge/IRF_epsAT2_aggregate.csv';
filenameAT4 = '~/brazil/irf_dsge/IRF_epsAT4_aggregate.csv';


%filenameAN = '~/brazil/irf_dsge/IRF_epsAN_aggregate.csv';

filenameAN0 = '~/brazil/irf_dsge/IRF_epsAN0_aggregate.csv';
filenameAN2 = '~/brazil/irf_dsge/IRF_epsAN2_aggregate.csv';
filenameAN4 = '~/brazil/irf_dsge/IRF_epsAN4_aggregate.csv';

 
%filenameAC = '~/brazil/irf_dsge/IRF_epsAC_aggregate.csv';
 
filenameAC0 = '~/brazil/irf_dsge/IRF_epsAC0_aggregate.csv';
filenameAC2 = '~/brazil/irf_dsge/IRF_epsAC2_aggregate.csv';
filenameAC4 = '~/brazil/irf_dsge/IRF_epsAC4_aggregate.csv';

 
csvwrite(filenameCN0, IRF_CN0);
csvwrite(filenameCN2, IRF_CN2);
csvwrite(filenameCN4,IRF_CN4);
 
csvwrite(filenamer,IRF_r);

%csvwrite(filenameAT,IRF_AT);
csvwrite(filenameAT0,IRF_AT0);
csvwrite(filenameAT2,IRF_AT2)
csvwrite(filenameAT4,IRF_AT4)
  
%csvwrite(filenameAN,IRF_AN);
csvwrite(filenameAN0,IRF_AN0);
csvwrite(filenameAN2,IRF_AN2);
csvwrite(filenameAN4,IRF_AN4);
 
%csvwrite(filenameAC,IRF_AC);
csvwrite(filenameAC0,IRF_AC0);
csvwrite(filenameAC2,IRF_AC2);
csvwrite(filenameAC4,IRF_AC4); 
}




%============================ Commodity Price Shock ===============================
figure(1)

cdd=csvread(filenameCN0);

% Rounding the numbers with 5 decimal:
clear rows;
clear cols;

rows = rows(cdd);
cols = columns(cdd);

for i = 1:rows
  for j = 1:cols
	    cdd(i, j) = round(cdd(i, j)*100000)/100000;
  endfor
endfor

Time=size(cdd(:,1));
t=1:20;
t=t';
set(0,'defaultAxesFontName', 'Cambria Math')
n=12; %font size for labels
m=14; %font size for title
v=0.7; %line size
%local = 'NorthEast';
local = 'SouthEast';


%Position of R
z=7;
%cdd = cdd./cdd(1,z);
%cdd1 = cdd1./cdd1(1,z);


a=0; % Real GDP

% POSITION

subplot(241); 
plot(t, cdd(:,a+1),'kb',t, cdd(:,a+2),'--kr', 'LineWidth', v, t, cdd(:,a+3),'--kr','LineWidth',v);
title('Real Output','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(242); 
plot(t, cdd(:,a+4),'kb',t, cdd(:,a+5),'--kr', 'LineWidth', v, t, cdd(:,a+6),'--kr','LineWidth',v);
title('Real Consumption','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(243); 
plot(t, cdd(:,a+7),'kb',t, cdd(:,a+8),'--kr','LineWidth', v,  t, cdd(:,a+9),'--kr','LineWidth',v);
title('Real Investment','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
subplot(244); 
plot(t, cdd(:,a+10),'kb', t, cdd(:,a+11),'--kr','LineWidth', v, t, cdd(:,a+12),'--kr','LineWidth',v);
title('Trade Balance-to-Output ratio','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(245); 
plot(t, cdd(:,a+13),'kb', t, cdd(:,a+14),'--kr', 'LineWidth',v, t, cdd(:,a+15),'--kr','LineWidth',v);
title('Hours Worked','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(246); 
plot(t, cdd(:,a+16),'kb',t, cdd(:,a+17),'--kr', 'LineWidth', v, t, cdd(:,a+18),'--kr','LineWidth',v);
title('Debt Holdings','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')
   

subplot(247); 
plot(t, cdd(:,a+19),'kb',t, cdd(:,a+20),'--kr', 'LineWidth',v, t, cdd(:,a+21),'--kr','LineWidth',v);
title('Real Commodity Price','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(248); 
plot(t, cdd(:,a+22),'kb',t, cdd(:,a+23),'--kr', 'LineWidth', v, t, cdd(:,a+24),'--kr','LineWidth',v);
title('Real Interest Rate','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
h=gcf;
set(h,'PaperOrientation','landscape');
% set(h, 'PaperType', 'a4');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf','~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/IRF_epsCN0.pdf');




%==================================================================================



%================================ News Shocks 2 ===================================
figure(2)

cdd=csvread(filenameCN2);

% Rounding the numbers with 5 decimal:
clear rows;
clear cols;

rows = rows(cdd);
cols = columns(cdd);

for i = 1:rows
  for j = 1:cols
	    cdd(i, j) = round(cdd(i, j)*100000)/100000;
  endfor
endfor

Time=size(cdd(:,1));
t=1:20;
t=t';
set(0,'defaultAxesFontName', 'Cambria Math')
n=12; %font size for labels
m=14; %font size for title
v=0.7; %line size
%local = 'NorthEast';
local = 'SouthEast';


%Position of R
z=7;
%cdd = cdd./cdd(1,z);
%cdd1 = cdd1./cdd1(1,z);


a=0; % Real GDP

% POSITION

subplot(241); 
plot(t, cdd(:,a+1),'kb',t, cdd(:,a+2),'--kr', 'LineWidth', v, t, cdd(:,a+3),'--kr','LineWidth',v);
title('Real Output','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(242); 
plot(t, cdd(:,a+4),'kb',t, cdd(:,a+5),'--kr', 'LineWidth', v, t, cdd(:,a+6),'--kr','LineWidth',v);
title('Real Consumption','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(243); 
plot(t, cdd(:,a+7),'kb',t, cdd(:,a+8),'--kr','LineWidth', v,  t, cdd(:,a+9),'--kr','LineWidth',v);
title('Real Investment','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
subplot(244); 
plot(t, cdd(:,a+10),'kb', t, cdd(:,a+11),'--kr','LineWidth', v, t, cdd(:,a+12),'--kr','LineWidth',v);
title('Trade Balance-to-Output ratio','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(245); 
plot(t, cdd(:,a+13),'kb', t, cdd(:,a+14),'--kr', 'LineWidth',v, t, cdd(:,a+15),'--kr','LineWidth',v);
title('Hours Worked','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(246); 
plot(t, cdd(:,a+16),'kb',t, cdd(:,a+17),'--kr', 'LineWidth', v, t, cdd(:,a+18),'--kr','LineWidth',v);
title('Debt Holdings','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')
   

subplot(247); 
plot(t, cdd(:,a+19),'kb',t, cdd(:,a+20),'--kr', 'LineWidth',v, t, cdd(:,a+21),'--kr','LineWidth',v);
title('Real Commodity Price','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(248); 
plot(t, cdd(:,a+22),'kb',t, cdd(:,a+23),'--kr', 'LineWidth', v, t, cdd(:,a+24),'--kr','LineWidth',v);
title('Real Interest Rate','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
h=gcf;
set(h,'PaperOrientation','landscape');
% set(h, 'PaperType', 'a4');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf','~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/IRF_epsCN2.pdf');



%==================================================================================




%================================ News Shocks 4 ===================================
figure(3)

cdd=csvread(filenameCN4);

% Rounding the numbers with 5 decimal:
clear rows;
clear cols;

rows = rows(cdd);
cols = columns(cdd);

for i = 1:rows
  for j = 1:cols
	    cdd(i, j) = round(cdd(i, j)*100000)/100000;
  endfor
endfor

Time=size(cdd(:,1));
t=1:20;
t=t';
set(0,'defaultAxesFontName', 'Cambria Math')
n=12; %font size for labels
m=14; %font size for title
v=0.7; %line size
%local = 'NorthEast';
local = 'SouthEast';


%Position of R
z=7;
%cdd = cdd./cdd(1,z);
%cdd1 = cdd1./cdd1(1,z);


a=0; % Real GDP

% POSITION

subplot(241); 
plot(t, cdd(:,a+1),'kb',t, cdd(:,a+2),'--kr', 'LineWidth', v, t, cdd(:,a+3),'--kr','LineWidth',v);
title('Real Output','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(242); 
plot(t, cdd(:,a+4),'kb',t, cdd(:,a+5),'--kr', 'LineWidth', v, t, cdd(:,a+6),'--kr','LineWidth',v);
title('Real Consumption','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(243); 
plot(t, cdd(:,a+7),'kb',t, cdd(:,a+8),'--kr','LineWidth', v,  t, cdd(:,a+9),'--kr','LineWidth',v);
title('Real Investment','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
subplot(244); 
plot(t, cdd(:,a+10),'kb', t, cdd(:,a+11),'--kr','LineWidth', v, t, cdd(:,a+12),'--kr','LineWidth',v);
title('Trade Balance-to-Output ratio','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(245); 
plot(t, cdd(:,a+13),'kb', t, cdd(:,a+14),'--kr', 'LineWidth',v, t, cdd(:,a+15),'--kr','LineWidth',v);
title('Hours Worked','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(246); 
plot(t, cdd(:,a+16),'kb',t, cdd(:,a+17),'--kr', 'LineWidth', v, t, cdd(:,a+18),'--kr','LineWidth',v);
title('Debt Holdings','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')
   

subplot(247); 
plot(t, cdd(:,a+19),'kb',t, cdd(:,a+20),'--kr', 'LineWidth',v, t, cdd(:,a+21),'--kr','LineWidth',v);
title('Real Commodity Price','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(248); 
plot(t, cdd(:,a+22),'kb',t, cdd(:,a+23),'--kr', 'LineWidth', v, t, cdd(:,a+24),'--kr','LineWidth',v);
title('Real Interest Rate','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
h=gcf;
set(h,'PaperOrientation','landscape');
% set(h, 'PaperType', 'a4');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf','~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/IRF_epsCN4.pdf');



%==================================================================================



%=============================== interest rate shock ==============================
figure(4)

cdd=csvread(filenamer);

% Rounding the numbers with 5 decimal:
clear rows;
clear cols;

rows = rows(cdd);
cols = columns(cdd);

for i = 1:rows
  for j = 1:cols
	    cdd(i, j) = round(cdd(i, j)*100000)/100000;
  endfor
endfor

Time=size(cdd(:,1));
t=1:20;
t=t';
set(0,'defaultAxesFontName', 'Cambria Math')
n=12; %font size for labels
m=14; %font size for title
v=0.7; %line size
%local = 'NorthEast';
local = 'SouthEast';


%Position of R
z=7;
%cdd = cdd./cdd(1,z);
%cdd1 = cdd1./cdd1(1,z);


a=0; % Real GDP

% POSITION

subplot(241); 
plot(t, cdd(:,a+1),'kb',t, cdd(:,a+2),'--kr', 'LineWidth', v, t, cdd(:,a+3),'--kr','LineWidth',v);
title('Real Output','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(242); 
plot(t, cdd(:,a+4),'kb',t, cdd(:,a+5),'--kr', 'LineWidth', v, t, cdd(:,a+6),'--kr','LineWidth',v);
title('Real Consumption','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(243); 
plot(t, cdd(:,a+7),'kb',t, cdd(:,a+8),'--kr','LineWidth', v,  t, cdd(:,a+9),'--kr','LineWidth',v);
title('Real Investment','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
subplot(244); 
plot(t, cdd(:,a+10),'kb', t, cdd(:,a+11),'--kr','LineWidth', v, t, cdd(:,a+12),'--kr','LineWidth',v);
title('Trade Balance-to-Output ratio','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(245); 
plot(t, cdd(:,a+13),'kb', t, cdd(:,a+14),'--kr', 'LineWidth',v, t, cdd(:,a+15),'--kr','LineWidth',v);
title('Hours Worked','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(246); 
plot(t, cdd(:,a+16),'kb',t, cdd(:,a+17),'--kr', 'LineWidth', v, t, cdd(:,a+18),'--kr','LineWidth',v);
title('Debt Holdings','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')
   

subplot(247); 
plot(t, cdd(:,a+19),'kb',t, cdd(:,a+20),'--kr', 'LineWidth',v, t, cdd(:,a+21),'--kr','LineWidth',v);
title('Real Commodity Price','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(248); 
plot(t, cdd(:,a+22),'kb',t, cdd(:,a+23),'--kr', 'LineWidth', v, t, cdd(:,a+24),'--kr','LineWidth',v);
title('Real Interest Rate','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
h=gcf;
set(h,'PaperOrientation','landscape');
% set(h, 'PaperType', 'a4');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf','~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/IRF_epsr.pdf');


%==================================================================================


	

%====================== Productivity shock (Tradable sector) ======================
figure(5)

cdd=csvread(filenameAT);

% Rounding the numbers with 5 decimal:
clear rows;
clear cols;

rows = rows(cdd);
cols = columns(cdd);

for i = 1:rows
  for j = 1:cols
	    cdd(i, j) = round(cdd(i, j)*100000)/100000;
  endfor
endfor

Time=size(cdd(:,1));
t=1:20;
t=t';
set(0,'defaultAxesFontName', 'Cambria Math')
n=12; %font size for labels
m=14; %font size for title
v=0.7; %line size
%local = 'NorthEast';
local = 'SouthEast';


%Position of R
z=7;
%cdd = cdd./cdd(1,z);
%cdd1 = cdd1./cdd1(1,z);


a=0; % Real GDP

% POSITION

subplot(241); 
plot(t, cdd(:,a+1),'kb',t, cdd(:,a+2),'--kr', 'LineWidth', v, t, cdd(:,a+3),'--kr','LineWidth',v);
title('Real Output','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(242); 
plot(t, cdd(:,a+4),'kb',t, cdd(:,a+5),'--kr', 'LineWidth', v, t, cdd(:,a+6),'--kr','LineWidth',v);
title('Real Consumption','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(243); 
plot(t, cdd(:,a+7),'kb',t, cdd(:,a+8),'--kr','LineWidth', v,  t, cdd(:,a+9),'--kr','LineWidth',v);
title('Real Investment','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
subplot(244); 
plot(t, cdd(:,a+10),'kb', t, cdd(:,a+11),'--kr','LineWidth', v, t, cdd(:,a+12),'--kr','LineWidth',v);
title('Trade Balance-to-Output ratio','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(245); 
plot(t, cdd(:,a+13),'kb', t, cdd(:,a+14),'--kr', 'LineWidth',v, t, cdd(:,a+15),'--kr','LineWidth',v);
title('Hours Worked','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(246); 
plot(t, cdd(:,a+16),'kb',t, cdd(:,a+17),'--kr', 'LineWidth', v, t, cdd(:,a+18),'--kr','LineWidth',v);
title('Debt Holdings','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')
   

subplot(247); 
plot(t, cdd(:,a+25),'kb',t, cdd(:,a+26),'--kr', 'LineWidth',v, t, cdd(:,a+27),'--kr','LineWidth',v);
title('Productivity in Tradable Sector','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(248); 
plot(t, cdd(:,a+22),'kb',t, cdd(:,a+23),'--kr', 'LineWidth', v, t, cdd(:,a+24),'--kr','LineWidth',v);
title('Real Interest Rate','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
h=gcf;
set(h,'PaperOrientation','landscape');
% set(h, 'PaperType', 'a4');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf','~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/IRF_epsAT.pdf');


%==================================================================================





%==================== Productivity shock (Nontradable sector) =====================
figure(6)


cdd=csvread(filenameAN);

% Rounding the numbers with 5 decimal:
clear rows;
clear cols;

rows = rows(cdd);
cols = columns(cdd);

for i = 1:rows
  for j = 1:cols
	    cdd(i, j) = round(cdd(i, j)*100000)/100000;
  endfor
endfor

Time=size(cdd(:,1));
t=1:20;
t=t';
set(0,'defaultAxesFontName', 'Cambria Math')
n=12; %font size for labels
m=14; %font size for title
v=0.7; %line size
%local = 'NorthEast';
local = 'SouthEast';


%Position of R
z=7;
%cdd = cdd./cdd(1,z);
%cdd1 = cdd1./cdd1(1,z);


a=0; % Real GDP

% POSITION

subplot(241); 
plot(t, cdd(:,a+1),'kb',t, cdd(:,a+2),'--kr', 'LineWidth', v, t, cdd(:,a+3),'--kr','LineWidth',v);
title('Real Output','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(242); 
plot(t, cdd(:,a+4),'kb',t, cdd(:,a+5),'--kr', 'LineWidth', v, t, cdd(:,a+6),'--kr','LineWidth',v);
title('Real Consumption','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(243); 
plot(t, cdd(:,a+7),'kb',t, cdd(:,a+8),'--kr','LineWidth', v,  t, cdd(:,a+9),'--kr','LineWidth',v);
title('Real Investment','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
subplot(244); 
plot(t, cdd(:,a+10),'kb', t, cdd(:,a+11),'--kr','LineWidth', v, t, cdd(:,a+12),'--kr','LineWidth',v);
title('Trade Balance-to-Output ratio','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(245); 
plot(t, cdd(:,a+13),'kb', t, cdd(:,a+14),'--kr', 'LineWidth',v, t, cdd(:,a+15),'--kr','LineWidth',v);
title('Hours Worked','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(246); 
plot(t, cdd(:,a+16),'kb',t, cdd(:,a+17),'--kr', 'LineWidth', v, t, cdd(:,a+18),'--kr','LineWidth',v);
title('Debt Holdings','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')
   

subplot(247); 
plot(t, cdd(:,a+28),'kb',t, cdd(:,a+29),'--kr', 'LineWidth',v, t, cdd(:,a+30),'--kr','LineWidth',v);
title('Productivity in Nontradable Sector','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(248); 
plot(t, cdd(:,a+22),'kb',t, cdd(:,a+23),'--kr', 'LineWidth', v, t, cdd(:,a+24),'--kr','LineWidth',v);
title('Real Interest Rate','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
h=gcf;
set(h,'PaperOrientation','landscape');
% set(h, 'PaperType', 'a4');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf','~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/IRF_epsAN.pdf');


%==================================================================================



	
 
%======================= Productivity (Commodity Sector) ==========================
figure(7)

cdd=csvread(filenameAC);

% Rounding the numbers with 5 decimal:
clear rows;
clear cols;

rows = rows(cdd);
cols = columns(cdd);

for i = 1:rows
  for j = 1:cols
	    cdd(i, j) = round(cdd(i, j)*100000)/100000;
  endfor
endfor

Time=size(cdd(:,1));
t=1:20;
t=t';
set(0,'defaultAxesFontName', 'Cambria Math')
n=12; %font size for labels
m=14; %font size for title
v=0.7; %line size
%local = 'NorthEast';
local = 'SouthEast';


%Position of R
z=7;
%cdd = cdd./cdd(1,z);
%cdd1 = cdd1./cdd1(1,z);


a=0; % Real GDP

% POSITION

subplot(241); 
plot(t, cdd(:,a+1),'kb',t, cdd(:,a+2),'--kr', 'LineWidth', v, t, cdd(:,a+3),'--kr','LineWidth',v);
title('Real Output','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(242); 
plot(t, cdd(:,a+4),'kb',t, cdd(:,a+5),'--kr', 'LineWidth', v, t, cdd(:,a+6),'--kr','LineWidth',v);
title('Real Consumption','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(243); 
plot(t, cdd(:,a+7),'kb',t, cdd(:,a+8),'--kr','LineWidth', v,  t, cdd(:,a+9),'--kr','LineWidth',v);
title('Real Investment','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
subplot(244); 
plot(t, cdd(:,a+10),'kb', t, cdd(:,a+11),'--kr','LineWidth', v, t, cdd(:,a+12),'--kr','LineWidth',v);
title('Trade Balance-to-Output ratio','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(245); 
plot(t, cdd(:,a+13),'kb', t, cdd(:,a+14),'--kr', 'LineWidth',v, t, cdd(:,a+15),'--kr','LineWidth',v);
title('Hours Worked','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(246); 
plot(t, cdd(:,a+16),'kb',t, cdd(:,a+17),'--kr', 'LineWidth', v, t, cdd(:,a+18),'--kr','LineWidth',v);
title('Debt Holdings','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')
   

subplot(247); 
plot(t, cdd(:,a+31),'kb',t, cdd(:,a+32),'--kr', 'LineWidth',v, t, cdd(:,a+33),'--kr','LineWidth',v);
title('Productivity in Commodity Sector','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')


subplot(248); 
plot(t, cdd(:,a+22),'kb',t, cdd(:,a+23),'--kr', 'LineWidth', v, t, cdd(:,a+24),'--kr','LineWidth',v);
title('Real Interest Rate','FontSize', n);  
xlabel('Quarter');
set(gca,'YGrid','off')

   
h=gcf;
set(h,'PaperOrientation','landscape');
% set(h, 'PaperType', 'a4');
set(h,'PaperUnits','normalized');
set(h,'PaperPosition', [0 0 1 1]);
print(gcf, '-dpdf','~/Dropbox/Dissertacao/CODE_PAPER/code_dynare/baseline/version3.14/estimation/brazil/irf_dsge/IRF_epsAC.pdf');
