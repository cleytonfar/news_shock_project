function y = master_set_auxiliary_variables(y, x, params)
%
% Status : Computes static model for Dynare
%
% Warning : this file is generated automatically by Dynare
%           from model file (.mod)

y(69)=y(28);
y(70)=x(3);
y(71)=x(3);
y(72)=x(3);
y(73)=x(3);
y(74)=x(2);
y(75)=x(2);
y(76)=x(5);
y(77)=x(5);
y(78)=x(6);
y(79)=x(6);
y(80)=x(6);
y(81)=x(6);
y(82)=x(11);
y(83)=x(11);
y(84)=x(12);
y(85)=x(12);
y(86)=x(12);
y(87)=x(12);
y(88)=x(8);
y(89)=x(8);
y(90)=x(9);
y(91)=x(9);
y(92)=x(9);
y(93)=x(9);
