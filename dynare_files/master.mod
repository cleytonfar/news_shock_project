/**********************************************************************
* PAPER:    Commodity Prices and Business Cycles in Emerging Economies: 
* 	 		The Role of News Shocks
*
* AUTHORS:  Farias, C. and Silva, M.E.A. 
*
* YEAR:     2020
*
* JOURNAL:  Working paper
* 
* FILE:     master.mod
*
* DESCRIPTION:	This file performs bayesian estimation of the model with news
*		about future changes in commodity price and TFP processes. 
*                     	       
* DATE:     Jan 15, 2017.
*
* VERSION: This version estimates the model without observable
*          investment. 
* 
* Last Modified: February 28, 2020. 
*
**********************************************************************/





//---------------- DECLARING ENDOGEOUNS VARIABLES ------------------//
var
    C_T    	${c^T}$	    // Consumption of Tradable goods
    C_N    	${c^N}$     // Consumption of nontradable goods
    L_T    	${L^T}$     // Labor supply for Tradable sector
    L_N    	${L^N}$     // Labor supply for nontradable sector
    L_C    	${L^C}$     // Labor supply for commodity sector 
    D_H    	${d^H}$     // Debt for households
    I_T    	${I^T}$     // Investiment in tradable sector
    I_N    	${I^N}$     // Investiment in Nontradable sector
    I_C    	${I^C}$     // Investiment in Commodity sector
    lambda 	${\lambda}$ // Lagrange multiplier

    A_T    	${A^T}$ // Productivity factor in tradable sector
    K_T    	${K^T}$  // Capital Demand in Tradable sector
    CM_T   	${CM^T}$ // Commodity demand in Tradable sector
    Y_T    	${Y^T}$  // Output in Tradable sector
    D_T    	${D^T}$  // Debt in Tradable sector
    TB_T   	${TB^N}$ // Trade balance in Tradable sector

    A_N    	${A^N}$  // Productivity factor Nontradable sector
    K_N    	${K^N}$  // Capital Demand in Nontradable sector
    Y_N    	${Y^N}$  // Debt holdings in Nontradable sector
    D_N    	${D^N}$  // Debt in NonTradable sector
    
    A_C    	${A^C}$    // Productivity factor Commodity sector
    K_C    	${K^C}$    // Capital Demand in Commodity sector
    Y_C    	${Y^C}$    // Output in Commodity sector
    D_C    	${D^C}$    // Debt holdings in Commodity sector
    TB_C   	${TB^C}$   // Trade balance in Commodity sector
    reer    	${reer}$   // real exchange rate
    P_N    	${P^N}$    // Price of Nontradable Goods
    P_C    	${P^{CM}}$ // Price of Commodity goods
    mu_T   	${\mu^N}$  // Price of capital for tradable sector
    mu_N   	${\mu^N}$  // price of capital for nontradable sector
    mu_C   	${\mu^N}$  // price of capital for commodity sector
    W_T    	${w^T}$    // wage rate in tradable sector
    W_N    	${w^N}$    // wage rate in nontradable sector
    W_C    	${w^{CM}}$ // wage rate in commodity sector
    r      	${r}$      // real country interest rate 

    Y      	${Y}$      // Aggregate Output
    C      	${C}$      // Aggregate Consumption
    I      	${I}$      // Aggregate Investment
    TB     	${TB}$     // Aggregate trade balance
    tb_y   	${TB_Y}$   // Trade balance to output ratio
    L      	${L}$      // Aggregate hours worked
    D  		${D}$ 	   // Debt position

    y_obs       ${\widetilde{y^{obs}}}$
    c_obs       ${\widetilde{c^{obs}}}$

//   i_obs

    p_C_obs     ${\widetilde{p_C^{obs}}}$
    R_obs       ${\widetilde{R^{obs}}}$
    tb_y_obs    ${\widetilde{tb_y^{obs}}}$

    Y_T_hat     ${\widetilde{Y_T}}$
    Y_N_hat     ${\widetilde{Y_N}}$
    Y_C_hat     ${\widetilde{Y_C}}$

    L_T_hat     ${\widetilde{L_T}}$
    L_N_hat     ${\widetilde{L_N}}$
    L_C_hat     ${\widetilde{L_C}}$

    I_T_hat     ${\widetilde{I_T}}$
    I_N_hat     ${\widetilde{I_N}}$
    I_C_hat     ${\widetilde{I_C}}$


    Y_hat       ${\widetilde{Y}}$	  // Output deviation from steady state
    C_hat       ${\widetilde{C}}$         // Consumption deviation from steady state
    I_hat       ${\widetilde{I}}$         // Investment deviation from steady state
    L_hat       ${\widetilde{L}}$         // Hours worked deviation from steady state
    tb_y_hat    ${\widetilde{TB_Y}}$      // trade balance-to-output ratio from steady state
    D_hat       ${\widetilde{D}}$

    P_C_hat     ${\widetilde{P^{CM}}}$
    r_hat       ${\widetilde{r}}$
    A_T_hat     ${\widetilde{A^{T}}}$
    A_N_hat     ${\widetilde{A^{N}}}$
    A_C_hat     ${\widetilde{A^{CM}}}$
    reer_hat    ${\widetilde{reer}}$
 	;

	
		  
//---------------- DECLARING EXOGENOUS VARIABLES -------------------//
varexo 	eps_C_news0 	 ${\epsilon^{0, CM}}$ 
       	eps_C_news2 	 ${\epsilon_{-2}^{news, CM}}$
       	eps_C_news4 	 ${\epsilon_{-4}^{news, CM}}$

	eps_AT_news0     ${\epsilon^{0, AT}$
	eps_AT_news2     ${\epsilon_{-2}^{news, AT}$
	eps_AT_news4     ${\epsilon_{-4}^{news, AT}$

	eps_AN_news0     ${\epsilon^{0, AN}$
	eps_AN_news2     ${\epsilon_{-2}^{news, AN}$
	eps_AN_news4     ${\epsilon_{-4}^{news, AN}$

	eps_AC_news0     ${\epsilon^{0, AC}$    
	eps_AC_news2     ${\epsilon_{-2}^{news, AC}$
	eps_AC_news4     ${\epsilon_{-4}^{news, AN}$

     	eps_r       	 ${\zeta^{r}}$ 
	;


//------------- DECLARING THE PARAMETERS OF THE MODEL----------------//
parameters 	bbeta				${\beta}$         // discount factor
           	b				${b}$             // internal habit formation
		vvarphi			 	${\varphi}$       // parameter CES aggregator
	  	ssigma				${\sigma}$        // risk aversion
	   	oomega_T			${\omega^{T}}$    // frish elasticity
	   	oomega_N			${\omega^{N}}$    // frish elasticity
	  	oomega_C			${\omega^{CM}}$   // frish elasticity
		cchi				${\chi}$          // parameter CES aggregator

	   	pphi_T				${\phi^{T}}$      // parameter ACC
	   	pphi_N				${\phi^{N}}$      // parameter ACC 
	   	pphi_C				${\phi^{CM}}$     // parameter ACC 

		ttheta_T                        ${\theta^{T}}$    // parameter ACL (on households)
		ttheta_N                        ${\theta^{N}}$    // parameter ACL (on households)
		ttheta_C                        ${\theta^{C}}$    // parameter ACL (on households)

		iiota_T				${\iota^{T}}$     // parameter ACL (on firms)
		iiota_N				${\iota^{N}}$     // parameter ACL (on firms)
		iiota_C				${\iota^{C}}$     // parameter ACL (on firms)

	   	ddelta				${\delta}$        // depretiation
	   	aalpha_T			${\alpha^{T}}$    // parameter production function
	   	aalpha_N			${\alpha^{N}}$    // parameter production function
	   	aalpha_C			${\alpha^{CM}}$   // parameter production function
	   	ggamma_T			${\gamma^{T}}$    // parameter production function
	   	eeta_T				${\eta^{T}}$      // fraction total expending
	   	eeta_N				${\eta^{N}}$      // fraction total expending
	   	eeta_C				${\eta^{CM}}$     // fraction total expending
	   	v_C				${\text{v}^{C}}$  // parameter AR interest rate
	   	v_D				${\text{v}^{d}}$  // parameter AR interest rate

	   	rrho1_C				${\rho_1^{C}}$    // parameter AR(2) commodity price 
	   	rrho2_C				${\rho_2^{C}}$    // parameter AR(2) commodity price
 
	   	rrho_r				${\rho^{r}}$      // parameter AR interest rate
	   	rrho_AN			 	${\theta^{AN}}$   // parameter AR productivity
	   	rrho_AT			 	${\theta^{AT}}$   // parameter AR productivity
	   	rrho_AC			 	${\theta^{AC}}$   // parameter AR productivity
	   	r_ss				${r_{ss}}$        // ss interest rate
	   	;

				 
// Parameter Calibration:

 vvarphi = 0.5;
 ssigma = 2;
 oomega_T = 1.455;
 oomega_N = 1.455;
 oomega_C = 1.455;
 cchi = 0.35;
 ddelta = 0.025;
 aalpha_T = 0.35;
 aalpha_N = 0.25;
 aalpha_C = 0.35;
 ggamma_T = 0.05;
 r_ss = 0.015;
 bbeta = 1/(1 + r_ss);

 rrho1_C = 1.40;
 rrho2_C = -0.49;

 ttheta_T = 0; // estimation without ACL on households
 ttheta_N = 0; // estimation without ACL on households
 ttheta_C = 0; // estimation without ACL on households

 iiota_T = 4.6;
 iiota_N = 9.3;
 iiota_C = 10.3;

// Parameters that will be estimated: 

 eeta_T = 1.9;
 eeta_N = 2.1;
 eeta_C = 2.5;

 v_C = -0.014;
 v_D = 0.077;

 rrho_r = 0.8;
 rrho_AT = 0.87;
 rrho_AN = 0.84;
 rrho_AC = 0.88;

 b = 0.45;

 pphi_T = 4.6;
 pphi_N = 9.3;
 pphi_C = 10.3;

  			 

	
//----------------- DECLARATION OF THE MODEL -----------------------//
model;

//---------------------  Households --------------------------------//

// FOC's:

//   1. Lagrange multiplier (marginal utility): 
lambda = (cchi*((C/C_T)^(1/vvarphi)))*(((C - b*C(-1) - ((L_T^oomega_T)/oomega_T) - ((L_N^oomega_N)/oomega_N) - ((L_C^oomega_C)/oomega_C))^(-ssigma)) - b*bbeta*((C(+1) - b*C - ((L_T(+1)^oomega_T)/oomega_T) - ((L_N(+1)^oomega_N)/oomega_N) - ((L_C(+1)^oomega_C)/oomega_C))^(-ssigma)));


//   2. Price of nontradable goods:
lambda*P_N = ((1 - cchi)*((C/C_N)^(1/vvarphi)))*(((C - b*C(-1) - ((L_T^oomega_T)/oomega_T)  - ((L_N^oomega_N)/oomega_N) - ((L_C^oomega_C)/oomega_C))^(-ssigma)) - b*bbeta*((C(+1) - b*C - ((L_T(+1)^oomega_T)/oomega_T)  - (L_N(+1)^oomega_N)/oomega_N) - ((L_C(+1)^oomega_C)/oomega_C))^(-ssigma));


//   3. Euler equation: 
lambda = bbeta*(1 + r)*lambda(+1);


//   4. Labor FOC Tradable sector: 
//((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_T^(oomega_T - 1)) = lambda*(W_T);
//((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_T^(oomega_T - 1)) =  lambda*(ttheta_N*(L_T(+1) - L_T) + W_T);

((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_T^(oomega_T - 1)) = lambda*(W_T - ttheta_T*(L_T/(L_T(-1)) - 1)*(L_T/L_T(-1)) - (ttheta_T/2)*((L_T/(L_T(-1))) - 1)^2) + bbeta*lambda(+1)*ttheta_T*((L_T(+1)/L_T) - 1)*(L_T(+1)/L_T)^2;


//   5.Labor FOC NonTradable Sector:
// ((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_N^(oomega_N - 1)) = lambda*(W_N);
// ((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_N^(oomega_N - 1)) = lambda*(ttheta_N*(L_N(+1) - L_N) + W_N);

((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_N^(oomega_N - 1)) = lambda*(W_N - ttheta_N*(L_N/(L_N(-1)) - 1)*(L_N/L_N(-1)) - (ttheta_N/2)*((L_N/(L_N(-1))) - 1)^2) + bbeta*lambda(+1)*ttheta_N*((L_N(+1)/L_N) - 1)*(L_N(+1)/L_N)^2;


//   6. Labor FOC Commodity sector:  
// ((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_C^(oomega_C - 1)) = lambda*(W_C);
// ((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_C^(oomega_C - 1)) = lambda*(ttheta_C*(L_C(+1) - L_C) + W_C);

((C - b*C(-1) - (L_T^oomega_T)/oomega_T  - (L_N^oomega_N)/oomega_N - (L_C^oomega_C)/oomega_C)^(-ssigma))*(L_C^(oomega_C - 1)) = lambda*(W_C - ttheta_C*(L_C/(L_C(-1)) - 1)*(L_C/L_C(-1)) - (ttheta_C/2)*((L_C/(L_C(-1))) - 1)^2) + bbeta*lambda(+1)*ttheta_C*((L_C(+1)/L_C) - 1)*(L_C(+1)/L_C)^2;



//   7. Investment FOC  Tradable sector
// lambda*(1 + (pphi_T*(K_T - K_T(-1)))) = bbeta*lambda(+1)*(mu_T(+1) + (1 - ddelta) + (pphi_T*(K_T(+1) - K_T)));

lambda*(1 + pphi_T*((K_T/K_T(-1)) - 1)*(K_T/K_T(-1)) + (pphi_T/2)*((K_T/K_T(-1)) - 1)^2) = bbeta*lambda(+1)*( mu_T(+1) + (1 - ddelta) + pphi_T*(K_T(+1)/K_T - 1)*(K_T(+1)/K_T)^2);



//   8.  Investment FOC  nontradable sector
//lambda*(1 + (pphi_N*(K_N - K_N(-1)))) = bbeta*lambda(+1)*(mu_N(+1) + (1 - ddelta) + (pphi_N*(K_N(+1) - K_N)));

lambda*(1 + pphi_N*((K_N/K_N(-1)) - 1)*(K_N/K_N(-1)) + (pphi_N/2)*((K_N/K_N(-1)) - 1)^2) = bbeta*lambda(+1)*( mu_N(+1) + (1 - ddelta) + pphi_N*(K_N(+1)/K_N - 1)*(K_N(+1)/K_N)^2);




//   9.  Investment FOC  Commodity sector
// lambda*(1 + (pphi_C*(K_C - K_C(-1)))) = bbeta*lambda(+1)*(mu_C(+1) + (1 - ddelta) + (pphi_T*(K_C(+1) - K_C)));

lambda*(1 + pphi_C*((K_C/K_C(-1)) - 1)*(K_C/K_C(-1)) + (pphi_C/2)*((K_C/K_C(-1)) - 1)^2) = bbeta*lambda(+1)*( mu_C(+1) + (1 - ddelta) + pphi_C*(K_C(+1)/K_C - 1)*(K_C(+1)/K_C)^2);



//   10.  Law of motion for capital in tradable sector
K_T = (1 - ddelta)*K_T(-1) + I_T;


//   11. Law of motion for capital in nontradable sector
K_N = (1 - ddelta)*K_N(-1) + I_N;



//   12. Law of motion for capital in commodity sector
K_C = (1 - ddelta)*K_C(-1) + I_C;

//------------------------------------------------------------------//




//------------------------- Firms ----------------------------------//

// Tradable Sector: 

//   13. FOC labor demand:
//L_T*W_T*(1 + eeta_T*(r/(1 + r))) = (1 - ggamma_T - aalpha_T)*Y_T;
(1 - ggamma_T - aalpha_T)*Y_T/L_T - W_T*(1 + eeta_T*(r/(1 + r))) - (iiota_T/2)*(L_T/L_T(-1) - 1)^2 - iiota_T*(L_T/L_T(-1) - 1)*(L_T/L_T(-1)) + bbeta*(lambda(+1)/lambda)*iiota_T*(L_T(+1)/L_T - 1)*((L_T(+1)/L_T)^2) = 0;


//   14. FOC capital demand:
K_T(-1)*mu_T*(1 + eeta_T*(r/(1 + r))) = aalpha_T*Y_T;


//   15. FOC commodity goods:
CM_T*P_C*(1 + eeta_T*(r/(1 + r))) = ggamma_T*Y_T;


//   16. Production Function
Y_T = A_T*(K_T(-1)^aalpha_T)*(CM_T^ggamma_T)*(L_T^(1 - aalpha_T - ggamma_T));


//   17. Debt holdings:
 D_T(+1) - (1 + r)*D_T = eeta_T*(((1/(1 + r(+1)))*(W_T(+1)*L_T(+1) + mu_T(+1)*K_T + P_C(+1)*CM_T(+1))) - (W_T*L_T + mu_T*K_T(-1) + P_C*CM_T));
// D_T - (1 + r(-1))*D_T(-1) = eeta_T*(((1/(1 + r))*(W_T*L_T + mu_T*K_T(-1) + P_C*CM_T)) - (W_T(-1)*L_T(-1) + mu_T(-1)*K_T(-2) + P_C(-1)*CM_T(-1)));

//------------------------------------------------------------------//



// Nontradable Sector:

//   18. FOC labor demand:
// L_N*W_N*(1 + eeta_N*(r/(1 + r))) = (1 - aalpha_N)*Y_N*P_N;
(1 - aalpha_N)*Y_N*P_N/L_N  - W_N*(1 + eeta_N*(r/(1 + r))) - (iiota_N/2)*(L_N/L_N(-1) - 1)^2 - iiota_N*(L_N/L_N(-1) - 1)*(L_N/L_N(-1)) + bbeta*(lambda(+1)/lambda)*iiota_N*(L_N(+1)/L_N - 1)*(L_N(+1)/L_N)^2 = 0;



//   19. FOC capital demand:
K_N(-1)*mu_N*(1 + eeta_N*(r/(1 + r))) = aalpha_N*Y_N*P_N;


//   20. Production Function
Y_N = A_N*(K_N(-1)^aalpha_N)*(L_N^(1 - aalpha_N));



//   21. Debt holdings:
 D_N(+1) - (1 + r)*D_N = eeta_N*(((1/(1 + r(+1)))*(W_N(+1)*L_N(+1) + mu_N(+1)*K_N)) - (W_N*L_N + mu_N*K_N(-1)));
// D_N - (1 + r)*D_N(-1) = eeta_N*(((1/(1 + r))*(W_N*L_N + mu_N*K_N(-1))) - (W_N(-1)*L_N(-1) + mu_N(-1)*K_N(-2)));
//---------------------------------------------------------------------------------



// Commodity Sector:

//   22. FOC labor demand:
// L_C*W_C*(1 + eeta_C*(r/(1 + r))) = (1 - aalpha_C)*P_C*Y_C;
 (1 - aalpha_C)*P_C*Y_C/L_C - W_C*(1 + eeta_C*(r/(1 + r))) - (iiota_C/2)*(L_C/L_C(-1) - 1)^2 - iiota_C*(L_C/L_C(-1) - 1)*(L_C/L_C(-1)) + bbeta*(lambda(+1)/lambda)*iiota_C*(L_C(+1)/L_C - 1)*(L_C(+1)/L_C)^2 = 0;


//   23. FOC capital demand:
K_C(-1)*mu_C*(1 + eeta_C*(r/(1 + r))) = aalpha_C*P_C*Y_C;


//   24. Production Function
Y_C = A_C*(K_C(-1)^aalpha_C)*(L_C^(1 - aalpha_C));


//   25. Debt holdings:
 D_C(+1) - (1 + r)*D_C = eeta_C*(((1/(1 + r(+1)))*(W_C(+1)*L_C(+1) + mu_C(+1)*K_C)) - (W_C*L_C + mu_C*K_C(-1)));
// D_C - (1 + r)*D_C(-1) = eeta_C*((1/(1 + r))*(W_C*L_C + mu_C*K_C(-1)) - (W_C(-1)*L_C(-1) + mu_C(-1)*K_C(-2)));

//------------------------------------------------------------------//





//----------------------- Market Clearing --------------------------//

//   26. Nontradable Sector: 
Y_N = C_N;



//   27. Tradable Sector:
// Y_T = C_T + I_T + I_N + I_C + ((pphi_T/2)*((K_T - K_T(-1))^2)) + ((pphi_N/2)*((K_N - K_N(-1))^2)) + ((pphi_C/2)*((K_C - K_C(-1))^2)) + TB_T;

Y_T = C_T + I_T + I_N + I_C + (pphi_T/2)*((K_T/K_T(-1)) - 1)^2 + (pphi_N/2)*((K_N/K_N(-1)) - 1)^2 + (pphi_C/2)*((K_C/K_C(-1)) - 1)^2 + TB_T;


//   28. Commodity Sector:
P_C*Y_C = P_C*CM_T + TB_C;


//   29. Aggregate Trade Balance:
TB = TB_T + TB_C;


//   30. Aggregate Debt-holdings:
D = D_H + D_T + D_N + D_C;


//   31. 
TB - r(-1)*D(-1) = -(D - D(-1));


//   32. Trade Balance to output ratio: 
tb_y = TB/Y;

// 32B. REER

reer = (cchi^vvarphi + ((1 - cchi)^vvarphi)*(P_N^(1 - vvarphi)))^(1/(1 - vvarphi));

//------------------------------------------------------------------//



//-------------------- Aggregate Variables -------------------------//

//   33. Aggregate Output:
Y = Y_T + P_N*Y_N + TB_C;


//   34. Aggregate Investment:
I = I_T + I_N + I_C;

// D and TB are already defined.


//   35. Aggregate consumption
C = (cchi*(C_T^((vvarphi - 1)/vvarphi)) + (1 - cchi)*(C_N^((vvarphi - 1)/vvarphi)))^(vvarphi/(vvarphi - 1));



//   36. Aggregate hours worked
L = L_T + L_N + L_C;

//---------------------------------------------------------------------------------



//---------------------------- Exogenous Processes --------------------------------

//   37. Commodity Price process around its steady state:
log(P_C/steady_state(P_C)) = rrho1_C*log(P_C(-1)/steady_state(P_C)) + rrho2_C*log(P_C(-2)/steady_state(P_C)) + eps_C_news0 + eps_C_news2(-2) + eps_C_news4(-4);


//   38.  World interest rate process around its steady state:
r = (1 - rrho_r)*steady_state(r) + rrho_r*r(-1) + v_D*(exp(D(-1) - steady_state(D)) - 1) + v_C*(exp(P_C - steady_state(P_C)) - 1) + eps_r;

//   39-41. Productivity process around its steady state:
log(A_T/steady_state(A_T)) = rrho_AT*log(A_T(-1)/steady_state(A_T)) + eps_AT_news0 + eps_AT_news2(-2) + eps_AT_news4(-4);
log(A_C/steady_state(A_C)) = rrho_AC*log(A_C(-1)/steady_state(A_C)) + eps_AC_news0 + eps_AC_news2(-2) + eps_AC_news4(-4);
log(A_N/steady_state(A_N)) = rrho_AN*log(A_N(-1)/steady_state(A_N)) + eps_AN_news0 + eps_AN_news2(-2) + eps_AN_news4(-4);



// Linking variables:
y_obs = log(Y) - log(steady_state(Y));
c_obs = log(C) - log(steady_state(C));

//i_obs = log(I) - log(steady_state(I));

p_C_obs = log(P_C) - log(steady_state(P_C));
R_obs = r - steady_state(r);
tb_y_obs = tb_y - steady_state(tb_y);




// 42-52 . Deviation from steady state: 

Y_T_hat = log(Y_T) - log(steady_state(Y_T));
Y_N_hat = log(Y_N) - log(steady_state(Y_N));
Y_C_hat = log(Y_C) - log(steady_state(Y_C));

L_T_hat = log(L_T) - log(steady_state(L_T));
L_N_hat = log(L_N) - log(steady_state(L_N));
L_C_hat = log(L_C) - log(steady_state(L_C));

I_T_hat = log(I_T) - log(steady_state(I_T));
I_N_hat = log(I_N) - log(steady_state(I_N));
I_C_hat = log(I_C) - log(steady_state(I_C));


Y_hat = log(Y) - log(steady_state(Y));
C_hat = log(C) - log(steady_state(C));
I_hat = log(I) - log(steady_state(I));
L_hat = log(L) - log(steady_state(L));
tb_y_hat = tb_y - steady_state(tb_y);
D_hat = D - steady_state(D);


A_T_hat = log(A_T) - log(steady_state(A_T));
A_N_hat = log(A_N) - log(steady_state(A_N));
A_C_hat = log(A_C) - log(steady_state(A_C));
P_C_hat = log(P_C) - log(steady_state(P_C));
r_hat = r - steady_state(r);
reer_hat = log(reer) - log(steady_state(reer)); 

end;
//------------------------------------------------------------------//


//------------- Declaring observable variables ---------------------//
varobs y_obs c_obs R_obs p_C_obs tb_y_obs;


 
// LEMBRAR QUE SE EXCLUIR ALGUMA OBSERVAVEL, COMENTAR AS EQUACOES
// REFERENTES A ELA AQUI NESSA FILE E NO STEADY_STATE FILE;


//---------------------- Declaring priors --------------------------//
// PARAMETER, PRIOR_DIST, PRIOR_MEAN, PRIOR_STDERR;
estimated_params;

b, beta_pdf, 0.5, 0.1;
 
rrho_r, , , 0.9, beta_pdf, 0.5, 0.2, , 0.9;
rrho_AT, , , 0.9, beta_pdf, 0.5, 0.2, , 0.9;
rrho_AN, , , 0.9, beta_pdf, 0.5, 0.2, , 0.9;
rrho_AC, , , 0.9, beta_pdf, 0.5, 0.2, , 0.9;

//pphi_T, gamma_pdf, 10, 5;
//pphi_N, gamma_pdf, 10, 5;
//pphi_C, gamma_pdf, 10, 5;

v_D, uniform_pdf, , , 0.0001, 5;
v_C, uniform_pdf, , , -5, 0;

eeta_T, gamma_pdf, 2, 1;
eeta_N, gamma_pdf, 2, 1;
eeta_C, gamma_pdf, 2, 1;

//ttheta_T, gamma_pdf, 10, 5;
//ttheta_N, gamma_pdf, 10, 5;
//ttheta_C, gamma_pdf, 10, 5;

iiota_T, gamma_pdf, 10, 5;
iiota_N, gamma_pdf, 10, 5;
iiota_C, gamma_pdf, 10, 5;

// standard deviation of shocks:
stderr eps_r, inv_gamma_pdf, 0.10, inf; 

stderr eps_AT_news0, inv_gamma_pdf, 0.10, inf;
stderr eps_AT_news2, gamma_pdf, 0.0408, 0.01;
stderr eps_AT_news4, gamma_pdf, 0.0408, 0.01;

stderr eps_AN_news0, inv_gamma_pdf, 0.10, inf;
stderr eps_AN_news2, gamma_pdf, 0.0408, 0.01;
stderr eps_AN_news4, gamma_pdf, 0.0408, 0.01;

stderr eps_AC_news0, inv_gamma_pdf, 0.10, inf;
stderr eps_AC_news2, gamma_pdf, 0.0408, 0.01;
stderr eps_AC_news4, gamma_pdf, 0.0408, 0.01;

stderr eps_C_news0,  inv_gamma_pdf, 0.10, inf;
stderr eps_C_news2,  gamma_pdf, 0.0408, 0.01;
stderr eps_C_news4,  gamma_pdf, 0.0408, 0.01;

end;


 
//---------------------------- STEADY STATE ------------------------//
steady(solve_algo = 1, maxit = 5000);
check;

// write_latex_original_model;
// write_latex_dynamic_model;
// write_latex_parameter_table;

//------------------------------------------------------------------//
//-------------------- Stochastic Simulation -----------------------//

shocks;

var eps_C_news0; stderr  0.0681;
var eps_C_news2; stderr  0.022;
var eps_C_news4; stderr  0.0456;

var eps_AT_news0; stderr 0.026;
var eps_AT_news2; stderr 0.007;
var eps_AT_news4; stderr 0.0172;

var eps_AN_news0; stderr 0.0192;
var eps_AN_news2; stderr 0.0056;
var eps_AN_news4; stderr 0.0061;

var eps_AC_news0; stderr 0.0451;
var eps_AC_news2; stderr 0.0196;
var eps_AC_news4; stderr 0.0221;

var eps_r; stderr 0.0121;

end;


/*
stoch_simul(order=1, irf=20)  
                Y_hat C_hat I_hat L_hat tb_y_hat D_hat reer_hat P_N 
                              ;
*/


//-------------------------------- ESTIMATION -------------------------------------
 
/*
// First run to find the mode and mh_jscale:
estimation(datafile = 'brazil_cycle_quadratic.mat',
	   plot_priors = 0,

           mode_compute = 6, //(Monte-Carlo algorithm),
           optim = ('AcceptanceRateTarget', 0.30),
           
//           mode_compute = 9, // (Covariance Matrix Adaptation Evolution Strategy (CMA-ES) algorithm)
//           optim = ('TolX', 1e-7, 'TolFun', 1e-7),

//	     mode_compute = 10,  // Simpsa algorithm (combination of the non-linear simplex and simulated annealing)
//           optim = ('TolX', 1e-7, 'TolFun', 1e-7),

//           mode_check,
        
	   mh_replic = 30000, // Number of rep. for Metropolis Hasting algorithm
	   mh_nblocks = 2,    // Number of parallel chains for the MH algo.
           mh_drop = 0.5, 
           //mh_init_scale = 4.5,   // The scale used for the jumping distribution
	   		      // in MH alg. this option must be TUNED to to obtain, 
 			      // an acceptance of 0.25.
	   mh_conf_sig = 0.9, // Confidence interval used for the computation of
	   	       	      // prior and posterior statistics like: parameter 
			      // distributions, prior/posterior moments, 
			      // conditional variance decomposition, 
			      // impulse response functions, 
	   bayesian_irf,      // Triggers computation of the posterior dist.of IRF.
	   irf = 40,
  	   tex) 
                Y_T_hat Y_N_hat Y_C_hat
                L_T_hat L_N_hat L_C_hat
                I_T_hat I_N_hat I_C_hat

                P_C_hat r_hat A_T_hat A_N_hat A_C_hat
                Y_hat C_hat I_hat L_hat tb_y_hat D_hat;
*/




/*
// The actual run with the mh_jscale calculated in the previous run:
estimation(datafile= 'brazil_cycle_quadratic.mat',
           mode_compute = 0,
           mode_file = 'master_mode.mat',
           load_mh_file,
           mh_replic = 100000,
           mh_nblocks = 2,
           mh_drop = 0.8,
           mh_jscale = 0.20239, 
	   mh_conf_sig = 0.68, 
 	   plot_priors = 0,
           bayesian_irf, 
           irf = 40,
           conf_sig = 0.68, 
           tex) 
                Y_T_hat Y_N_hat Y_C_hat 
                L_T_hat L_N_hat L_C_hat
                I_T_hat I_N_hat I_C_hat

                P_C_hat r_hat A_T_hat A_N_hat A_C_hat
                Y_hat C_hat I_hat L_hat tb_y_hat D_hat;
*/




// load the estimation results:
estimation(datafile = 'brazil_cycle_quadratic.mat',
           	mode_compute = 0,
           	mode_file = 'master_mode.mat',
           	load_mh_file,
           	mh_replic = 0,
           	mh_drop = 0.8,
           	mh_jscale = 0.20239,
	  	plot_priors = 0,
           	bayesian_irf,
           	irf = 20,
           	conf_sig = 0.68,
           	moments_varendo, // triggers the computation of the posterior 
	   		     	 // distribution of the theoretical moments of the 
			     	 // endogenous variables. 
	   	conditional_variance_decomposition = [2 4 6 8 16 32 40],  
           	tex) 
                Y_T_hat Y_N_hat Y_C_hat
                L_T_hat L_N_hat L_C_hat
                I_T_hat I_N_hat I_C_hat

                P_C_hat r_hat A_T_hat A_N_hat A_C_hat
                Y_hat C_hat I_hat L_hat tb_y_hat D_hat reer_hat;



// SHOCK decomposition:
shock_groups(name=group1);
commodity = eps_C_news0;
news_commodity  = eps_C_news2, eps_C_news4;
TFP = eps_AT_news0, eps_AN_news0, eps_AC_news0;
news_TFP = eps_AT_news2, eps_AT_news4, eps_AN_news2, eps_AN_news4, eps_AC_news2, eps_AC_news4;
interest_rate = eps_r;
end;

shock_decomposition(parameter_set=posterior_mode, use_shock_groups=group1) Y C I L;




// identification;
// dynare_sensitivity;



/*
// FEVD simulation:
stoch_simul(order = 1, // approximation about the steady state;
          //hp_filter=1600, //Uses HP filter with  before computing moments. 

           irf = 20, // number of periods plotted BY IRF 
           //relative_irf, // Requests the computation of normalized IRFs
                         // in percentage of the standard error of each shock.
                         
          //nocorr, // it will not print a matrix of correlation
          //nofunction, // it will supress printing the state space coefficients
          //nomoments, // it will supress printing of moments
          //noprint, // it will supress printinf of any output at all
         // periods=500, // it will simulate data and take moments from these data,
                         // instead of theoretical moments.
         // drop=200 // number of obs to drop from the simulations. by default, 
                   // dynare drop the first 100 values.
          //replic=1, //number of simulated series used to compute the IRF's
                      //default=1, if order=1, and 50 otherwise.
         conditional_variance_decomposition = 40
         ) 	Y_hat 
	 	C_hat
		I_hat
		L_hat
		tb_y_hat
		A_T_hat
		A_N_hat
		A_C_hat
		P_C_hat
		r_hat
		;

/*

//---------------------------- HISTORICAL DECOMPOSITION ---------------------------
// shock_decomposition(parameter_set = posterior_mode) [Y C I L];


