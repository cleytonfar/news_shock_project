function [ys,check] = master_steadystate(ys,exo)
% function [ys,check] = master_steadystate(ys,exo)
% computes the steady state for the master.mod and uses a numerical
% solver to do so. 
% 
% INPUTS: 
%   - ys        [vector] vector of initial values for the steady state of
%                        the endogenous variables
%
%   - exo       [vector] vector of values for the exogenous variables
%
% OUTPUT: 
%   - ys        [vector] vector of steady state values for the the endogenous
%                        variables.
%  
%   - check     [scalar] set to 0 if steady state computation worked and to
%                        1 of not (allows to impos restriction on parameters)

global M_ 

% read out parameters to access them with their name:	 
NumberOfParameters = M_.param_nbr;
for ii = 1:NumberOfParameters
  paramname = deblank(M_.param_names(ii,:));
  eval([ paramname ' = M_.params(' int2str(ii) ');']);
end

% initialize indicator
check = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Enter model equations here %%%%%%%%%%%%%%%%%%%%%%%%%%


% Productivity factor for each sector:
A_Tss = 1;
A_Nss = 1;
A_Css = 1;

% Interest rate in the steady state:
r =  r_ss;


	
%===================== Defining the TARGETS ================
% Trade balance-to-output ratio = 1%:
tby = 0.01; 

% Trade balance of commodity sector-to-output ratio = 10%
tby_C = 0.1; 
%===========================================================


% Capital rate in the steady state:
mu_Tss = (1/bbeta) - (1 - ddelta);
mu_Nss = mu_Tss;
mu_Css = mu_Tss;


% set options for numerical solver: 
options=optimset('Display','Final','TolX',1e-12,'TolFun',1e-12, 'MaxIter', 500000);


% Initial values for the solver:
init = [2; 9; 0.12; 1.2; 9.5; 0.9; 0.7; 14; 1.2; 0.6; 0.2];


[FVAL y exitflag] = fsolve(@(t)(F(t, b, bbeta, vvarphi, ssigma, oomega_T, oomega_N, oomega_C, cchi, ddelta, aalpha_T, aalpha_N, aalpha_C, ggamma_T, pphi_T, pphi_N, pphi_C, r, mu_Tss, mu_Nss, mu_Css, eeta_N, eeta_T, eeta_C, A_Nss, A_Tss, A_Css, tby, tby_C)), init, options);


if exitflag <1
    % indicate the SS computation was not sucessful; this would also be detected by Dynare
    % setting the indicator here shows how to use this functionality to
    % filter out parameter draws
    check = 1; % set failure indicator
    return; % return without updating steady states
end

reer = FVAL(1);
KL_N = FVAL(2);
L_Nss = FVAL(3);
P_Nss = FVAL(4);
KL_C = FVAL(5);
W_Css = FVAL(6);
theta = FVAL(7);
KL_T = FVAL(8);
W_Tss = FVAL(9);
P_Css = FVAL(10);
Dss = FVAL(11);



% Using labor supply conditions:
L_Css = (((1 - b*bbeta)*W_Css)/reer)^(1/(oomega_C - 1));
L_Tss = (((1 - b*bbeta)*W_Tss)/reer)^(1/(oomega_T - 1));
W_Nss = (reer*(L_Nss^(oomega_N - 1)))/(1 - b*bbeta);


% Capital stock in the steady state is found by
K_Css = KL_C*L_Css;
K_Tss = KL_T*L_Tss;
K_Nss = KL_N*L_Nss;


% Using the definition of optimal commodity demand:
CM_Tss = ((ggamma_T*mu_Tss)/(P_Css*aalpha_T))*K_Tss;


% Production in the steady state:
Y_Tss = A_Tss*(K_Tss^aalpha_T)*(CM_Tss^ggamma_T)*(L_Tss^(1 - ggamma_T - aalpha_T));
Y_Css = A_Css*(K_Css^aalpha_C)*(L_Css^(1 - aalpha_C));
Y_Nss = A_Nss*(K_Nss^aalpha_N)*(L_Nss^(1 - aalpha_N));


% Trade balance for Commodity and Tradable sector:
TBss = r*Dss;
TB_Css = P_Css*(Y_Css - CM_Tss);
TB_Tss = TBss - TB_Css;


% Investment in the steady state for each sector: 
I_Tss = ddelta*K_Tss;
I_Css = ddelta*K_Css;
I_Nss = ddelta*K_Nss;


% Consumption of Tradable and nontradable goods:
C_Nss = Y_Nss;
C_Tss = C_Nss*(((cchi/(1 - cchi))*P_Nss)^vvarphi);
Css = (cchi*(C_Tss^((vvarphi - 1)/vvarphi)) + (1 - cchi)*(C_Nss^((vvarphi - 1)/vvarphi)))^(vvarphi/(vvarphi-1));


% Debt for NonTradable Sector:
D_Nss = (1/(1+r))*eeta_N*(W_Nss*L_Nss + mu_Nss*K_Nss); 


% Debt for Commodity Sector:
D_Css = (1/(1+r))*eeta_C*(W_Css*L_Css + mu_Css*K_Css);


% Debt for Tradable Sector:
D_Tss = (1/(1+r))*eeta_T*(W_Tss*L_Tss + mu_Tss*K_Tss + P_Css*CM_Tss);


% Debt for Households: 
D_Hss = Dss - D_Tss - D_Nss - D_Css; 
%D_Hss = (W_Tss*L_Tss + W_Nss*L_Nss + W_Css*L_Css + mu_Tss*K_Tss + mu_Nss*K_Nss + mu_Css*K_Css - C_Tss - P_Nss*C_Nss - I_Tss - I_Nss - I_Css)/r; 


% Total Output in the numeraire tradable units: 
Yss = Y_Tss + P_Nss*Y_Nss + TB_Css;


% Total Investment in tradable unit:
Iss = I_Tss + I_Nss + I_Css;


% Total hours worked: 
Lss = L_Tss + L_Nss + L_Css;



% variáveis auxiliares para cálculo do lambdass: 
UtCtss = ((1 - b)*Css - (L_Tss^oomega_T)/oomega_T - (L_Nss^oomega_N)/oomega_N - (L_Css^oomega_C)/oomega_C)^(-ssigma);
A_CTss = cchi*((Css/C_Tss)^(1/vvarphi));
A_CNss = (1 - cchi)*((Css/C_Nss)^(1/vvarphi));


% lagrange multiplier:
lambdass = (1 - b*bbeta)*A_CTss*UtCtss;



% Targets: trade-balance to output ratio = 1%
%          trade balance of commodity goods-to-output ratio = 10%
    
tby = (r*Dss)/Yss;
tby_C = (P_Css*(Y_Css - CM_Tss))/Yss;



  
% Retornando as Variáveis em log:

% 1. 
%C_T = log(C_Tss);
C_T = (C_Tss);

% 2.
%C_N = log(C_Nss);
C_N = (C_Nss);

%3.
%I_T = log(I_Tss);
I_T = (I_Tss);

%4.
%I_N = log(I_Nss);
I_N = (I_Nss);

%5.
 %I_C = log(I_Css);
I_C = (I_Css);

%6.
 %L_T = log(L_Tss);
L_T = (L_Tss);

%7.
 %L_N = log(L_Nss);
L_N = (L_Nss);

%8.
 %L_C = log(L_Css);
L_C = L_Css;

%9.
D_H = D_Hss;

%10.
 %lambda = log(lambdass);
lambda = lambdass;

%11. 
 %A_N = log(A_Nss);
A_N = A_Nss;


%12.
 %K_N = log(K_Nss);
K_N = K_Nss;

%13.
 %Y_N = log(Y_Nss);
Y_N = Y_Nss;

%14. 
D_N = D_Nss;

%15. 
 %A_C = log(A_Css);
A_C = A_Css;


%16.
 %K_C = log(K_Css);
K_C = K_Css;


%17.
 %Y_C = log(Y_Css);
Y_C = Y_Css;

%18.
D_C = D_Css;

%19.
TB_C = TB_Css;

%20.
 %A_T = log(A_Tss);
A_T = (A_Tss);


%21.
%K_T = log(K_Tss);
K_T = K_Tss;

%22.
 %CM_T = log(CM_Tss);
CM_T = CM_Tss;

%23.
 %Y_T = log(Y_Tss);
Y_T = Y_Tss;

%24.
D_T = D_Tss;

%25.
TB_T = TB_Tss;

%26. 
 %P_N = log(P_Nss);
P_N = P_Nss;

%27.
 %P_C = log(P_Css);
P_C = P_Css;

%28.
 %mu_T = log(mu_Tss);
mu_T = mu_Tss;

%29.
 %mu_N = log(mu_Nss);
mu_N = mu_Nss;

%30. 
%mu_C = log(mu_Css);
mu_C = mu_Css;

%31.
%W_T = log(W_Tss);
W_T = W_Tss;

%32.
 %W_N = log(W_Nss);
W_N = W_Nss;

%33.
 %W_C = log(W_Css);
W_C = W_Css;

%34.
r = r;

%35. 
%Y = log(Yss);
Y = Yss;

%36. 
 %C = log(Css);
C = Css;

%37.
 %I = log(Iss);
I = Iss;

%38. 
 %L = log(Lss);
L = Lss;

%39.

TB = TBss;

%40.
D = Dss;

%41
tb_y =  tby;


Y_T_hat = 0;
Y_N_hat = 0;
Y_C_hat = 0;

L_T_hat = 0;
L_N_hat = 0;
L_C_hat = 0;

I_T_hat = 0;
I_N_hat = 0;
I_C_hat = 0;


Y_hat   = 0;
C_hat   = 0;
I_hat   = 0;
L_hat   = 0;
tb_y_hat = 0;
		
D_hat	 = 0;

P_C_hat  = 0;
r_hat    = 0;
A_T_hat	 = 0;
A_N_hat	 = 0;
A_C_hat = 0;
reer_hat =0;
y_obs = 0;
c_obs = 0;
p_C_obs = 0;
R_obs = 0;
tb_y_obs = 0;
%%i_obs = 0;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% end own model equations %%%%%%%%%%%%%%%%%%%%%%%%%%%




for iter = 1:length(M_.params) %update parameters set in the file
  eval([ 'M_.params(' num2str(iter) ') = ' M_.param_names(iter,:) ';' ])
end



NumberOfEndogenousVariables = M_.orig_endo_nbr; %auxiliary variables are set automatically
for ii = 1:NumberOfEndogenousVariables
  varname = deblank(M_.endo_names(ii,:));
  eval(['ys(' int2str(ii) ') = ' varname ';']);
end

