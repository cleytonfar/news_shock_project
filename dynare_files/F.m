function y = F(x, b, bbeta, vvarphi, ssigma, oomega_T, oomega_N, oomega_C, cchi, ddelta, aalpha_T, aalpha_N, aalpha_C, ggamma_T, pphi_T, pphi_N, pphi_C, r, mu_Tss, mu_Nss, mu_Css, eeta_N, eeta_T, eeta_C, A_Nss, A_Tss, A_Css, tby, tby_C)

reer = x(1);
KL_N = x(2);
L_Nss = x(3);
Pss_N = x(4);
KL_C = x(5);
W_Css = x(6);
theta = x(7);
KL_T = x(8);
W_Tss = x(9);
P_Css = x(10);
Dss = x(11);

% Nontradable sector: {solve numerically for reer, Kss_N/L_Nss, L_Nss and Pss_N}
y(1) = reer - (cchi^vvarphi + ((1 - cchi)^vvarphi)*(Pss_N^(1 - vvarphi)))^(1/(1 - vvarphi));

y(2) = mu_Nss - ((aalpha_N*Pss_N*A_Nss*(KL_N^(aalpha_N - 1)))/(1 + eeta_N*(r/(1+r))));

y(3) = (1 + eeta_N*(r/(1+r)))*(reer/(1 - b*bbeta))*(L_Nss^(oomega_N - 1)) - (1 - aalpha_N)*Pss_N*A_Nss*(KL_N^aalpha_N);


y(4) = Pss_N - ((1 - cchi)/cchi)*((((theta*A_Tss*(KL_T^(aalpha_T + ggamma_T))*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))) - (ddelta*KL_T*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))) - (ddelta*KL_N*L_Nss) - (ddelta*KL_C*((((1 - b*bbeta)/reer)*W_Css)^(1/(oomega_C - 1)))) - ((r*Dss) - (P_Css*((A_Css*(KL_C^aalpha_C)*((((1 - b*bbeta)/reer)*W_Css)^(1/(oomega_C - 1)))) - ((ggamma_T*(theta*A_Tss*(KL_T^(aalpha_T + ggamma_T))*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))))/(P_Css*(1 + eeta_T*(r/(1 + r)))))))))/(A_Nss*(KL_N^aalpha_N)*L_Nss))^(1/vvarphi));

y(5) = W_Css - (((1 - aalpha_C)*(P_Css)*A_Css)/(1 + eeta_C*(r/(1+r))))*((KL_C)^aalpha_C);

y(6) = KL_C - ((aalpha_C*(P_Css)*A_Css)/(mu_Css*(1 + eeta_C*(r/(1+r)))))^(1/(1 - aalpha_C));

y(7) = W_Tss - ((1 - aalpha_T - ggamma_T)/(1 + eeta_T*(r/(1+r))))*((theta)*A_Tss)*((KL_T)^(aalpha_T + ggamma_T));

y(8) = KL_T - (((theta)*aalpha_T*A_Tss)/(mu_Tss*(1 + eeta_T*(r/(1 + r)))))^(1/(1 - aalpha_T - ggamma_T));

y(9) = theta - ((ggamma_T*mu_Tss)/((P_Css)*aalpha_T))^ggamma_T;

y(10) = Dss - (((tby)*(((theta)*A_Tss*(KL_T^(aalpha_T + ggamma_T))*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))) + (Pss_N)*(A_Nss*(KL_N^aalpha_N)*(L_Nss)) + (P_Css*((A_Css*(KL_C^aalpha_C)*((((1 - b*bbeta)/reer)*W_Css)^(1/(oomega_C - 1)))) - ((ggamma_T*((theta)*A_Tss*(KL_T^(aalpha_T + ggamma_T))*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))))/(P_Css*(1 + eeta_T*(r/(1 + r)))))))))/r);

y(11) = P_Css - ((tby_C)*(((theta)*A_Tss*(KL_T^(aalpha_T + ggamma_T))*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))) + (Pss_N)*(A_Nss*(KL_N^aalpha_N)*(L_Nss)) + (P_Css*((A_Css*(KL_C^aalpha_C)*((((1 - b*bbeta)/reer)*W_Css)^(1/(oomega_C - 1)))) - ((ggamma_T*((theta)*A_Tss*(KL_T^(aalpha_T + ggamma_T))*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))))/(P_Css*(1 + eeta_T*(r/(1 + r)))))))))/((A_Css*(KL_C^aalpha_C)*((((1 - b*bbeta)/reer)*W_Css)^(1/(oomega_C - 1)))) - ((ggamma_T*(theta*A_Tss*(KL_T^(aalpha_T + ggamma_T))*((((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1)))))/(P_Css*(1 + eeta_T*(r/(1 + r))))));


end


% Yss = ((theta)*A_Tss*(KL_T^(aalpha_T + ggamma_T))*(L_Tss)) + (P_Nss)*(A_Nss*(KL_N^aalpha_N)*(L_Nss)) + (P_Css*((A_Css*(KL_C^aalpha_C)*(L_Css)) - ((ggamma_T*((theta)*A_Tss*(KL_T^(aalpha_T + ggamma_T))*(L_Tss)))/(P_Css*(1 + eeta_T*(r/(1 + r)))))))

% C_T = (Y_Tss) - (I_Tss) - (I_Nss) - (I_Css) - (TB_Tss)

% C_N = Y_Nss

% Y_Nss = A_N*(KL_N^aalpha_N)*(L_Nss)

% Y_Tss = theta*A_Tss*(KL_T^(aalpha_T + ggamma_T))*(L_Tss)

% I_Tss = ddelta*KL_T*(L_Tss)

% I_Css = ddelta*KL_C*(L_Css)

% I_Nss = ddelta*KL_N*L_Nss

% L_Tss = (((1 - b*bbeta)/reer)*W_Tss)^(1/(oomega_T - 1))

% L_Css = (((1 - b*bbeta)/reer)*W_Css)^(1/(oomega_C - 1))

% TB_Tss = TBss - TB_Css

% TB_Css = Pss_C*((Y_Css) - (CM_Tss))

% Y_Css = A_Css*(KL_C^aalpha_C)*(L_Css)

% CM_Tss = (ggamma_T*(Y_Tss))/(Pss_C*(1 + eeta_T*(r/(1 + r))))


